package com.delepues.buyer;

import com.delepues.buyer.http.apimodel.Category;
import com.delepues.buyer.landing.LandingFragmentMVP;
import com.delepues.buyer.landing.LandingPresenter;
import com.delepues.buyer.landing.LandingRepository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import rx.Observable;
import rx.Scheduler;
import rx.android.plugins.RxAndroidPlugins;
import rx.android.plugins.RxAndroidSchedulersHook;
import rx.observers.TestSubscriber;
import rx.schedulers.Schedulers;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyCollectionOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by azapata on 10/12/17.
 */

public class LandingPresenterTest {

    LandingFragmentMVP.Model mockLoginModel;
    LandingFragmentMVP.View mockView;
    LandingRepository repository;
    LandingPresenter presenter;
    String country;
    ArrayList<Category> data;

    @Before
    public void setup() {
        RxAndroidPlugins.getInstance().registerSchedulersHook(new RxAndroidSchedulersHook() {
            @Override
            public Scheduler getMainThreadScheduler() {
                return Schedulers.immediate();
            }
        });
        mockLoginModel = mock(LandingFragmentMVP.Model.class);

        country = "nic";
        data = new ArrayList<>();
        data.add(new Category("1", "Supermercados", "url"));
        data.add(new Category("2", "Restaurantes", "url"));
        data.add(new Category("3", "Farmacias", "url"));
        //testSubscriber

        mockView = mock(LandingFragmentMVP.View.class);
        repository = mock(LandingRepository.class);
        presenter = new LandingPresenter(mockLoginModel);
        presenter.setView(mockView);
    }

    @Test
    public void shouldShowListOfCategories() throws Exception {
        when(mockLoginModel.getCategories(country)).thenReturn(Observable.just(data));//Observable.<ArrayList<Category>>empty());
        presenter.getCategories(country);
        //verify(mockView, times(1)).showProgress();

        //verify model interactions
        verify(mockLoginModel, times(1)).getCategories(country);

        //verify view interactions
        verify(mockView, times(1)).showCategories(data);
        verify(mockView, never()).errorCategories("error");
    }

    @Test
    public void shouldShowErrorCategoriesEmpty() throws Exception {
        when(mockLoginModel.getCategories(country)).thenReturn(Observable.just(new ArrayList<Category>()));
        presenter.getCategories(country);
        //verify(mockView, times(1)).showProgress();

        //verify model interactions
        verify(mockLoginModel, times(1)).getCategories(country);

        //verify view interactions
        verify(mockView, times(1)).errorEmpty();
        verify(mockView, never()).errorCategories("error");
    }

    @Test
    public void shouldShowErrorCategories() throws Exception {
        Throwable someException = new Exception();
        when(mockLoginModel.getCategories(country)).thenReturn(Observable.<ArrayList<Category>>error(someException));
        presenter.getCategories(country);
        //verify(mockView, times(1)).showProgress();

        //verify model interactions
        verify(mockLoginModel, times(1)).getCategories(country);

        //verify view interactions
        verify(mockView, times(1)).errorCategories(someException.getMessage());
        verify(mockView, never()).showCategories(data);
        verify(mockView, never()).errorEmpty();
    }

    @After
    public void tearDown() {
        RxAndroidPlugins.getInstance().reset();
    }
}
