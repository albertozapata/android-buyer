package com.delepues.buyer.profile;

import com.delepues.buyer.http.BuyerApiService;
import com.delepues.buyer.http.apimodel.Address;
import com.delepues.buyer.http.apimodel.Profile;
import com.delepues.buyer.http.apimodel.ResponseCart;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public class ProfileRepository implements Repository {
    BuyerApiService buyerApiService;

    public ProfileRepository(BuyerApiService buyerApiService) {
        this.buyerApiService = buyerApiService;
    }

    @Override
    public Observable<Profile> getResultsFromNetwork(String buyerId) {
        return buyerApiService.profile(buyerId);
    }

    @Override
    public Observable<Profile> getResultsFromLocal(String buyerId) {
        Profile profile = new Profile();
        profile.user_profile_id = "111";
        profile.user_id = "222";
        profile.buyer_id = "333";
        profile.user_state_id = "444";
        profile.username = "azapata";
        profile.first_name = "Julio";
        profile.last_name = "Garcia";
        profile.phone = "84176593";
        profile.email = "f.cjulio5@gmail.com";
        Address address = new Address();
        address.address = "Km 2 1/2 Carretera Norte, Ajax Delgado 40vrs Abajo #17";
        address.address_id = "555";
        address.latitude = 0.0;
        address.longitude = 0.0;
        ArrayList<Address> addresses = new ArrayList<>();
        addresses.add(address);
        addresses.add(address);
        addresses.add(address);
        profile.addresses = addresses;
        return Observable.just(profile);
    }

    @Override
    public Observable<ResponseCart> getResultsFromNetworkAddress(Address address, String buyerId) {
        return buyerApiService.addAddress(address, buyerId);
    }
}
