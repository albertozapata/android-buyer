package com.delepues.buyer.profile;

import com.delepues.buyer.http.apimodel.Address;
import com.delepues.buyer.http.apimodel.Buyer;
import com.delepues.buyer.http.apimodel.BuyerResponse;
import com.delepues.buyer.http.apimodel.Profile;
import com.delepues.buyer.http.apimodel.ResponseCart;

import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public interface ProfileFragmentMVP {
    interface View {
        void showProfile(Profile profile);

        void errorProfile(String s);

        void addressAdded();

        void errorAddress(String s);

        void showProgress();

        void hideProgress();

        void errorEmpty();
    }

    interface Presenter {
        void profile(String buyerId);

        void addAddress(Address address,String buyerId);

        void rxUnsubscribe();

        void setView(ProfileFragmentMVP.View view);
    }

    interface Model {
        Observable<Profile> profile(String buyerId);
        Observable<ResponseCart> addAddress(Address address,String buyerId);
    }
}
