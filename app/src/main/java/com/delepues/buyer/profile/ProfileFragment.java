package com.delepues.buyer.profile;


import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.delepues.buyer.R;
import com.delepues.buyer.address.AddressActivity;
import com.delepues.buyer.http.apimodel.Address;
import com.delepues.buyer.http.apimodel.BuyerResponse;
import com.delepues.buyer.http.apimodel.Profile;
import com.delepues.buyer.register.RegisterActivity;
import com.delepues.buyer.register.RegisterFragmentMVP;
import com.delepues.buyer.root.App;
import com.delepues.buyer.store.dialog.ProfileAddressAdapter;
import com.delepues.buyer.utils.Constants;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.pixplicity.easyprefs.library.Prefs;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment implements ProfileFragmentMVP.View {
    @BindView(R.id.image_user_register)
    ImageView imageUser;
    @BindView(R.id.image_user_email)
    ImageView imageEmail;
    @BindView(R.id.image_phone_register)
    ImageView imagePhone;
    @BindView(R.id.image_add)
    ImageView imageAdd;

    @BindView(R.id.edit_username_register)
    TextInputEditText username;
    @BindView(R.id.edit_email_register)
    TextInputEditText email;
    @BindView(R.id.edit_phone_register)
    TextInputEditText phone;
    @BindView(R.id.list_addresses)
    ListView listView;
    @BindView(R.id.relative_address)
    RelativeLayout relativeLayout;

    ProfileAddressAdapter profileAddressAdapter;

    @Inject
    ProfileFragmentMVP.Presenter presenter;

    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getActivity().getApplication()).getComponent().inject(this);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        buildIcons();
        presenter.profile("");
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AddressActivity.class);
                startActivityForResult(intent, 2);// Activity is started with requestCode 2
            }
        });
    }

    private void buildIcons() {
        int color = getActivity().getResources().getColor(R.color.colorGray);
        Drawable drawable = new IconicsDrawable(getContext())
                .icon(FontAwesome.Icon.faw_user)
                .color(color)
                .sizeDp(24);
        imageUser.setImageDrawable(drawable);
        drawable = new IconicsDrawable(getContext())
                .icon(FontAwesome.Icon.faw_phone)
                .color(color)
                .sizeDp(24);
        imagePhone.setImageDrawable(drawable);
        drawable = new IconicsDrawable(getContext())
                .icon(FontAwesome.Icon.faw_envelope)
                .color(color)
                .sizeDp(24);
        imageEmail.setImageDrawable(drawable);
        drawable = new IconicsDrawable(getContext())
                .icon(FontAwesome.Icon.faw_plus_circle)
                .color(getActivity().getResources().getColor(R.color.colorPrimary))
                .sizeDp(24);
        imageAdd.setImageDrawable(drawable);
    }

    @Override
    public void showProfile(Profile profile) {
        username.setText(String.format("%s %s", profile.first_name, profile.last_name));
        email.setText(profile.email);
        phone.setText(profile.phone);
        profileAddressAdapter = new ProfileAddressAdapter(getContext(), profile.addresses);
        listView.setAdapter(profileAddressAdapter);
        setSizeListView(profile.addresses.size());
    }

    @Override
    public void errorProfile(String s) {

    }

    @Override
    public void addressAdded() {

    }

    @Override
    public void errorAddress(String s) {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void errorEmpty() {

    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.setView(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.rxUnsubscribe();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (profileAddressAdapter != null)
            presenter.profile("");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == 2) {
            Address address = data.getParcelableExtra(Constants.address);
            presenter.addAddress(address, "");
        }
    }

    private void setSizeListView(int size) {
        ListAdapter listadp = listView.getAdapter();
        if (listadp != null) {
            int totalHeight = 0;
            for (int i = 0; i < size; i++) {
                View listItem = listadp.getView(i, null, listView);
                listItem.measure(0, 0);
                totalHeight += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight + (listView.getDividerHeight() * (listadp.getCount() - 1));
            listView.setLayoutParams(params);
            listView.requestLayout();
        }
    }
}
