package com.delepues.buyer.profile;


import android.util.Log;

import com.delepues.buyer.http.apimodel.Address;
import com.delepues.buyer.http.apimodel.Profile;
import com.delepues.buyer.http.apimodel.ResponseCart;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by azapata on 10/12/17.
 */

public class ProfilePresenter implements ProfileFragmentMVP.Presenter {
    private ProfileFragmentMVP.View view;
    private Subscription subscription = null;
    private ProfileFragmentMVP.Model model;
    private Profile profile;
    private ResponseCart response;

    public ProfilePresenter(ProfileFragmentMVP.Model model) {
        this.model = model;
    }

    @Override
    public void profile(final String buyerId) {
        subscription = model.profile(buyerId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Profile>() {
                    @Override
                    public void onCompleted() {
                        if (profile == null) {
                            view.errorEmpty();
                        } else {
                            Log.e("DELIVAVA", profile.email);
                            view.showProfile(profile);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.errorProfile(e.getMessage());
                    }

                    @Override
                    public void onNext(Profile profileResponse) {
                        profile = profileResponse;
                    }
                });
    }

    @Override
    public void addAddress(Address address, String buyerId) {
        subscription = model.addAddress(address, buyerId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseCart>() {
                    @Override
                    public void onCompleted() {
                        if (response == null) {
                            view.errorEmpty();
                        } else {
                            view.addressAdded();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.errorAddress(e.getMessage());
                    }

                    @Override
                    public void onNext(ResponseCart responseCart) {
                        response = responseCart;
                    }
                });
    }

    @Override
    public void rxUnsubscribe() {
        if (subscription != null) {
            if (!subscription.isUnsubscribed()) {
                subscription.unsubscribe();
            }
        }
    }

    @Override
    public void setView(ProfileFragmentMVP.View view) {
        this.view = view;
    }
}
