package com.delepues.buyer.profile;

import com.delepues.buyer.http.BuyerApiService;


import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by azapata on 10/12/17.
 */
@Module
public class ProfileModule {
    @Provides
    public ProfileFragmentMVP.Presenter providesProfileFragmentPresenter(ProfileFragmentMVP.Model profileModel) {
        return new ProfilePresenter(profileModel);
    }

    @Provides
    public ProfileFragmentMVP.Model provideProfileFragmentModel(Repository repository) {
        return new ProfileModel(repository);
    }

    @Singleton
    @Provides
    public Repository provideRepo(BuyerApiService buyerApiService) {
        return new ProfileRepository(buyerApiService);
    }
}
