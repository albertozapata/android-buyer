package com.delepues.buyer.store.dialog


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.delepues.buyer.R
import com.delepues.buyer.http.apimodel.Address
import java.util.*


/**
 * Created by azapata on 11/5/17.
 */

class ProfileAddressAdapter(private val context: Context, val addresses: ArrayList<Address>) : BaseAdapter() {
    override fun getCount(): Int {
        return addresses.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItem(position: Int): Any {
        return addresses[position]
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        val viewHolder: ViewHolder
        var view = convertView
        if (convertView == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_address, parent, false)
            viewHolder = ViewHolder(view)
            view.tag = viewHolder
        } else {
            viewHolder = view?.tag as ViewHolder
        }

        val currentItem = getItem(position) as Address
        viewHolder.itemName.text = currentItem.address

        return view
    }

    private inner class ViewHolder(view: View) {
        internal var itemName: TextView = view.findViewById(R.id.text_item_address) as TextView
    }
}
