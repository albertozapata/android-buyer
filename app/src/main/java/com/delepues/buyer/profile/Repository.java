package com.delepues.buyer.profile;

import com.delepues.buyer.http.apimodel.Address;
import com.delepues.buyer.http.apimodel.Profile;
import com.delepues.buyer.http.apimodel.ResponseCart;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public interface Repository {
    Observable<Profile> getResultsFromNetwork(String buyerId);

    Observable<Profile> getResultsFromLocal(String buyerId);

    Observable<ResponseCart> getResultsFromNetworkAddress(Address address,String buyerId);
}
