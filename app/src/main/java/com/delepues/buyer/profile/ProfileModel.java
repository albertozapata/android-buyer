package com.delepues.buyer.profile;

import com.delepues.buyer.http.apimodel.Address;
import com.delepues.buyer.http.apimodel.Buyer;
import com.delepues.buyer.http.apimodel.BuyerResponse;
import com.delepues.buyer.http.apimodel.Profile;
import com.delepues.buyer.http.apimodel.ResponseCart;

import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public class ProfileModel implements ProfileFragmentMVP.Model {
    private Repository repository;

    public ProfileModel(Repository repository) {
        this.repository = repository;
    }

    @Override
    public Observable<Profile> profile(String buyerId) {
        return repository.getResultsFromLocal(buyerId);
    }

    @Override
    public Observable<ResponseCart> addAddress(Address address, String buyerId) {
        return repository.getResultsFromNetworkAddress(address,buyerId);
    }
}
