package com.delepues.buyer.store;

import com.delepues.buyer.http.BuyerApiService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by azapata on 10/12/17.
 */
@Module
public class StoreModule {
    @Provides
    public StoreFragmentMVP.Presenter providesLandingFragmentPresenter(StoreFragmentMVP.Model landingModel) {
        return new StorePresenter(landingModel);
    }

    @Provides
    public StoreFragmentMVP.Model provideLandingFragmentModel(Repository repository) {
        return new StoreModel(repository);
    }

    @Singleton
    @Provides
    public Repository provideRepo(BuyerApiService buyerApiService) {
        return new StoreRepository(buyerApiService);
    }
}
