package com.delepues.buyer.store;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.delepues.buyer.R;
import com.delepues.buyer.http.apimodel.Product;
import com.delepues.buyer.store.dialog.BuyDialogFragment2;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by azapata on 10/22/17.
 */

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.SingleItemRowHolder> {

    private ArrayList<Product> itemsList;
    private Context mContext;

    public ProductsAdapter(Context context, ArrayList<Product> itemsList) {
        this.itemsList = itemsList;
        this.mContext = context;
    }

    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_product_category, null);
        SingleItemRowHolder mh = new SingleItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(SingleItemRowHolder holder, int i) {
        final Product singleItem = itemsList.get(i);
        holder.textName.setText(singleItem.name);
        holder.buttonBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = ((AppCompatActivity) mContext).getSupportFragmentManager();
                BuyDialogFragment2 buyDialogFragment = BuyDialogFragment2.newInstance(singleItem, false);
                buyDialogFragment.show(fm, "fragment_edit_name");
            }
        });
        Picasso.with(mContext).load(singleItem.image_url).placeholder(mContext.getResources().getDrawable(R.drawable.product_placeholder)).into(holder.imageProduct);

    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {
        protected TextView textName;
        protected TextView textPrice;
        protected ImageView imageProduct;
        protected Button buttonBuy;


        public SingleItemRowHolder(View view) {
            super(view);
            this.textName = view.findViewById(R.id.text_item_product);
            this.imageProduct = view.findViewById(R.id.image_item_product);
            this.buttonBuy = view.findViewById(R.id.button_buy);
        }
    }

}
