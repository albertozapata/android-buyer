package com.delepues.buyer.store;

import com.delepues.buyer.http.apimodel.Category;
import com.delepues.buyer.http.apimodel.Product;
import com.delepues.buyer.http.apimodel.SectionDataModel;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public interface Repository {
    Observable<ArrayList<SectionDataModel>> getResultsFromNetwork(String branchId);
    Observable<ArrayList<SectionDataModel>> getResultsFromLocal(String branchId);
}
