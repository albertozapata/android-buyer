package com.delepues.buyer.store;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.delepues.buyer.R;
import com.delepues.buyer.http.apimodel.Branch;
import com.delepues.buyer.http.apimodel.SectionDataModel;
import com.delepues.buyer.root.App;
import com.delepues.buyer.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link StoreFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class StoreFragment extends Fragment implements StoreFragmentMVP.View {
    private Branch branch;

//    @BindView(R.id.toolbar)
//    Toolbar toolbar;

    @BindView(R.id.recycler_products_category)
    RecyclerView recyclerView;

    @BindView(R.id.header)
    ImageView header;
    @BindView(R.id.image_logo)
    ImageView logo;
    @BindView(R.id.relative_header)
    RelativeLayout relativeHeader;
    @Inject
    StoreFragmentMVP.Presenter presenter;


    public StoreFragment() {
        // Required empty public constructor
    }

    public static StoreFragment newInstance(Branch branchId) {
        StoreFragment fragment = new StoreFragment();
        Bundle args = new Bundle();
        args.putParcelable(Constants.branch, branchId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getActivity().getApplication()).getComponent().inject(this);
        if (getArguments() != null) {
            branch = getArguments().getParcelable(Constants.branch);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_store, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //setUpToolbar();
        Picasso.with(getContext()).load(branch.banner_url).placeholder(getResources().getDrawable(R.drawable.banner_placeholder)).into(header);
        Picasso.with(getContext()).load(branch.image_url).placeholder(getResources().getDrawable(R.drawable.banner_placeholder)).into(logo);
        presenter.getProducts(branch.store_branch_id);
        //        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//                if (dy > 0) {
//                    ResizeAnimation resizeAnimation = new ResizeAnimation(
//                            relativeHeader,
//                            80,
//                            160
//                    );
//                    resizeAnimation.setDuration(1000);
//                    relativeHeader.startAnimation(resizeAnimation);
//                } else {
////                    ResizeAnimation resizeAnimation = new ResizeAnimation(
////                            relativeHeader,
////                            160,
////                            80
////                    );
////                    resizeAnimation.setDuration(1000);
////                    relativeHeader.startAnimation(resizeAnimation);
//                }
//            }
//        });
    }

    @Override
    public void showProducts(ArrayList<SectionDataModel> sectionData) {
        RecyclerView.Adapter adapter = new SectionProductsAdapter(getActivity(), sectionData);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void errorProducts(String s) {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void errorEmpty() {

    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.setView(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.rxUnsubscribe();
    }

    private void setUpToolbar() {
        //((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.title_landing_fragment);
        //((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
    }

    public class ResizeAnimation extends Animation {
        final int targetHeight;
        View view;
        int startHeight;

        public ResizeAnimation(View view, int targetHeight, int startHeight) {
            this.view = view;
            this.targetHeight = targetHeight;
            this.startHeight = startHeight;
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            int newHeight = (int) (startHeight + targetHeight * interpolatedTime);
            //to support decent animation, change new heigt as Nico S. recommended in comments
            //int newHeight = (int) (startHeight+(targetHeight - startHeight) * interpolatedTime);
            view.getLayoutParams().height = newHeight;
            view.requestLayout();
        }

        @Override
        public void initialize(int width, int height, int parentWidth, int parentHeight) {
            super.initialize(width, height, parentWidth, parentHeight);
        }

        @Override
        public boolean willChangeBounds() {
            return true;
        }
    }

}
