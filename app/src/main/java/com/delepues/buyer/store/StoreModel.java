package com.delepues.buyer.store;

import com.delepues.buyer.http.apimodel.SectionDataModel;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public class StoreModel implements StoreFragmentMVP.Model {
    private Repository repository;

    public StoreModel(Repository repository) {
        this.repository = repository;
    }

    @Override
    public Observable<ArrayList<SectionDataModel>> getProducts(String branchId) {
        return repository.getResultsFromNetwork(branchId);
        //return repository.getResultsFromLocal(branchId);
    }
}
