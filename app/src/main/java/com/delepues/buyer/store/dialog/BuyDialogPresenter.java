package com.delepues.buyer.store.dialog;

import com.delepues.buyer.http.apimodel.DeleteProduct;
import com.delepues.buyer.http.apimodel.OrderShopping;
import com.delepues.buyer.http.apimodel.Product;
import com.delepues.buyer.http.apimodel.ResponseCart;
import com.delepues.buyer.http.apimodel.SectionDataModel;

import java.util.ArrayList;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by azapata on 10/12/17.
 */

public class BuyDialogPresenter implements BuyDialogFragmentMVP.Presenter {
    private BuyDialogFragmentMVP.View view;
    private Subscription subscription = null;
    private BuyDialogFragmentMVP.Model model;
    private ArrayList<OrderShopping> orderShoppings;
    private ResponseCart responseCart;

    public BuyDialogPresenter(BuyDialogFragmentMVP.Model model) {
        this.model = model;
    }


//    @Override
//    public void addProduct(Product product) {
//        boolean result = model.addProduct(product);
//        if(result)
//            view.addedProduct();
//        else
//            view.errorProducts("Error al guardar producto");
//    }

    @Override
    public void addProduct(final Product product, String buyerId) {
        subscription = model.addProduct(product, buyerId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ArrayList<OrderShopping>>() {
                    @Override
                    public void onCompleted() {
                        if (!orderShoppings.isEmpty()) {
                            model.addProductLocal(product);
                            view.addedProduct();
                        } else
                            view.errorProducts("No se pudo agregar el producto");
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.errorProducts(e.getMessage());
                    }

                    @Override
                    public void onNext(ArrayList<OrderShopping> orderShoppings1) {
                        orderShoppings = orderShoppings1;
                    }
                });
    }

    @Override
    public void deleteProduct(final DeleteProduct product, String buyerId) {
        subscription = model.deleteProduct(product, buyerId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseCart>() {
                    @Override
                    public void onCompleted() {
                        if (responseCart.status) {
                            model.deleteProductLocal(product.order_detail_id);
                            view.deletedProduct();
                        } else
                            view.errorProducts("No se pudo eliminar el producto");
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.errorProducts(e.getMessage());
                    }

                    @Override
                    public void onNext(ResponseCart response) {
                        responseCart = response;
                    }
                });
    }

    @Override
    public void rxUnsubscribe() {
        if (subscription != null) {
            if (!subscription.isUnsubscribed()) {
                subscription.unsubscribe();
            }
        }
    }

    @Override
    public void setView(BuyDialogFragmentMVP.View view) {
        this.view = view;
    }

}
