package com.delepues.buyer.store


import android.graphics.Rect
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import butterknife.ButterKnife
import com.delepues.buyer.R
import com.delepues.buyer.http.apimodel.Product
import com.delepues.buyer.utils.Constants
import java.util.*
import kotlinx.android.synthetic.main.fragment_products.*
import kotlin.collections.ArrayList
import android.support.v7.widget.RecyclerView
import com.delepues.buyer.store.ProductsFragment.GridSpacingItemDecoration






/**
 * A simple [Fragment] subclass.
 * Use the [ProductsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ProductsFragment : Fragment() {

    lateinit var products: ArrayList<Product>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let { products = it.getParcelableArrayList(Constants.product) }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_products, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        //val singleItem = (0..10).mapTo(ArrayList()) { Product("" + it, "Tomates", 12.50, "url", "Tomates Frescos", null) }

        val resId = R.anim.layout_animation_fall_down
        val animation = AnimationUtils.loadLayoutAnimation(activity, resId)
        val itemListDataAdapter = ProductsAdapter(activity, products)
        recycler_products.setHasFixedSize(true)
        recycler_products.itemAnimator = DefaultItemAnimator()
        recycler_products.layoutAnimation = animation

        recycler_products.layoutManager = GridLayoutManager(activity, 2)
        val spanCount = 2 // 3 columns
        val spacing = 20 // 50px
        val includeEdge = false
        recycler_products.addItemDecoration(GridSpacingItemDecoration(spanCount, spacing, includeEdge))

        recycler_products.adapter = itemListDataAdapter
        recycler_products.adapter.notifyDataSetChanged()
        recycler_products.scheduleLayoutAnimation()
    }

    companion object {
        fun newInstance(products: ArrayList<Product>): ProductsFragment {
            val fragment = ProductsFragment()
            val args = Bundle()
            args.putParcelableArrayList(Constants.product, products)
            fragment.arguments = args
            return fragment
        }
    }

    inner class GridSpacingItemDecoration(private val spanCount: Int, private val spacing: Int, private val includeEdge: Boolean) : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
            val position = parent.getChildAdapterPosition(view) // item position
            val column = position % spanCount // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing
                }
                outRect.bottom = spacing // item bottom
            } else {
                outRect.left = column * spacing / spanCount // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing // item top
                }
            }
        }
    }

}// Required empty public constructor
