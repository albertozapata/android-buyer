package com.delepues.buyer.store.dialog;

import com.delepues.buyer.http.BuyerApiService;
import com.delepues.buyer.store.StoreRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by azapata on 10/12/17.
 */
@Module
public class BuyDialogModule {
    @Provides
    public BuyDialogFragmentMVP.Presenter providesBuyDialogFragmentPresenter(BuyDialogFragmentMVP.Model landingModel) {
        return new BuyDialogPresenter(landingModel);
    }

    @Provides
    public BuyDialogFragmentMVP.Model provideBuyDialogFragmentModel(Repository repository) {
        return new BuyDialogModel(repository);
    }

    @Singleton
    @Provides
    public Repository provideRepo(BuyerApiService buyerApiService) {
        return new BuyDialogRepository(buyerApiService);
    }
}
