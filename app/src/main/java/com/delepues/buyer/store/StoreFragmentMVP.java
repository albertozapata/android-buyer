package com.delepues.buyer.store;

import com.delepues.buyer.http.apimodel.SectionDataModel;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public interface StoreFragmentMVP {
    interface View {
        void showProducts(ArrayList<SectionDataModel> sectionData);

        void errorProducts(String s);

        void showProgress();

        void hideProgress();

        void errorEmpty();
    }

    interface Presenter {
        void getProducts(String branchId);

        void rxUnsubscribe();

        void setView(StoreFragmentMVP.View view);
    }

    interface Model {
        Observable<ArrayList<SectionDataModel>> getProducts(String branchId);
    }
}
