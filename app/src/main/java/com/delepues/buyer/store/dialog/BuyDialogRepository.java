package com.delepues.buyer.store.dialog;

import com.delepues.buyer.http.BuyerApiService;
import com.delepues.buyer.http.apimodel.DeleteProduct;
import com.delepues.buyer.http.apimodel.Option;
import com.delepues.buyer.http.apimodel.OptionDetail;
import com.delepues.buyer.http.apimodel.OrderShopping;
import com.delepues.buyer.http.apimodel.Product;
import com.delepues.buyer.http.apimodel.ResponseCart;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public class BuyDialogRepository implements Repository {

    BuyerApiService buyerApiService;

    public BuyDialogRepository(BuyerApiService buyerApiService) {
        this.buyerApiService = buyerApiService;
    }

    @Override
    public boolean addProduct(final Product product) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                com.delepues.buyer.http.localmodel.Product productLocal = realm.createObject(com.delepues.buyer.http.localmodel.Product.class);
                productLocal.store_branch_product_id = product.store_branch_product_id;
                productLocal.name = product.name;
                productLocal.price = product.price;
                productLocal.image_url = product.image_url;
                productLocal.description = product.description;
                productLocal.qty = product.qty;
                productLocal.order_id = product.order_id;
                productLocal.order_detail_id = product.order_detail_id;
                RealmList<com.delepues.buyer.http.localmodel.Option> optionRealmList = new RealmList<>();

                for (Option option : product.options) {
                    com.delepues.buyer.http.localmodel.Option optionLocal = realm.createObject(com.delepues.buyer.http.localmodel.Option.class);
                    optionLocal.multichoice = option.multichoice;
                    optionLocal.name = option.name;
                    optionLocal.option_id = option.option_id;
                    optionLocal.required = option.required;
                    RealmList<com.delepues.buyer.http.localmodel.OptionDetail> optionDetailRealmList = new RealmList<>();

                    for (OptionDetail optionDetail : option.option_details) {
                        com.delepues.buyer.http.localmodel.OptionDetail optionDetailLocal = realm.createObject(com.delepues.buyer.http.localmodel.OptionDetail.class);
                        optionDetailLocal.name = optionDetail.name;
                        optionDetailLocal.option_detail_id = optionDetail.option_detail_id;
                        optionDetailLocal.price = optionDetail.price;
                        optionDetailLocal.selected = optionDetail.selected;
                        optionDetailRealmList.add(optionDetailLocal);
                    }
                    optionLocal.option_details = optionDetailRealmList;
                    optionRealmList.add(optionLocal);
                }
                productLocal.options = optionRealmList;
            }
        });
        return true;
    }

    @Override
    public boolean deleteProduct(String product) {
        Realm realm = Realm.getDefaultInstance();
        final RealmResults<com.delepues.buyer.http.localmodel.Product> results = realm.where(com.delepues.buyer.http.localmodel.Product.class).equalTo("order_detail_id", product).findAll();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                results.deleteAllFromRealm();
            }
        });
        return true;
    }

    @Override
    public Observable<ArrayList<OrderShopping>> getResultsFromNetwork(ArrayList<Product> products, String buyerId) {
        return buyerApiService.addProduct(products, buyerId);
    }

    @Override
    public Observable<ResponseCart> getResultsFromNetwork(DeleteProduct product, String buyerId) {
        return buyerApiService.deleteProduct(product, buyerId);
    }
}
