package com.delepues.buyer.store.dialog;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.delepues.buyer.R;
import com.delepues.buyer.http.apimodel.Option;
import com.delepues.buyer.http.apimodel.OptionDetail;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by azapata on 11/5/17.
 */

public class BuyProductsAdapter extends BaseAdapter {
    private ArrayList<Object> personArray;
    private LayoutInflater inflater;
    private static final int TYPE_PERSON = 0;
    private static final int TYPE_DIVIDER = 1;
    private BuyDialogFragment2 fragment;
    private Context context;

    public BuyProductsAdapter(Context context, ArrayList<Object> personArray, BuyDialogFragment2 fragment) {
        this.personArray = personArray;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.fragment = fragment;
    }

    @Override
    public int getCount() {
        return personArray.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return personArray.get(position);
    }

    @Override
    public int getViewTypeCount() {
        // TYPE_PERSON and TYPE_DIVIDER
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position) instanceof OptionDetail) {
            return TYPE_PERSON;
        }

        return TYPE_DIVIDER;
    }

    @Override
    public boolean isEnabled(int position) {
        return (getItemViewType(position) == TYPE_PERSON);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        int type = getItemViewType(position);
        if (convertView == null) {
            switch (type) {
                case TYPE_PERSON:
                    convertView = inflater.inflate(R.layout.item_buy_extra, parent, false);
                    break;
                case TYPE_DIVIDER:
                    convertView = inflater.inflate(R.layout.header_buy_extra, parent, false);
                    break;
            }
        }

        switch (type) {
            case TYPE_PERSON:
                final OptionDetail person = (OptionDetail) getItem(position);
                TextView name = convertView.findViewById(R.id.text_item_buy_extra);
                TextView price = convertView.findViewById(R.id.text_item_buy_price);
                CheckBox checkBox = convertView.findViewById(R.id.check_buy_extra);
                name.setText(person.name);
                if (person.price != 0)
                    price.setText(String.format(Locale.getDefault(), "%.2f", person.price));
                else
                    price.setText("");

                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        person.selected = isChecked;
                        personArray.set(position, person);
                        fragment.updateButton(person.price, isChecked);
                    }
                });
                checkBox.setChecked(person.selected);
                break;
            case TYPE_DIVIDER:
                TextView title = convertView.findViewById(R.id.text_buy_header);
                Option titleString = (Option) getItem(position);
                title.setText(titleString.name);
                break;
        }

        return convertView;
    }

    public ArrayList<Object> getPersonArray() {
        return personArray;
    }
}
