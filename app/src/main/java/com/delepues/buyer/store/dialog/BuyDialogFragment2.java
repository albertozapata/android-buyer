package com.delepues.buyer.store.dialog;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.delepues.buyer.R;
import com.delepues.buyer.http.apimodel.DeleteProduct;
import com.delepues.buyer.http.apimodel.Option;
import com.delepues.buyer.http.apimodel.OptionDetail;
import com.delepues.buyer.http.apimodel.Product;
import com.delepues.buyer.register.RegisterActivity;
import com.delepues.buyer.root.App;
import com.delepues.buyer.utils.Constants;
import com.pixplicity.easyprefs.library.Prefs;
import com.squareup.picasso.Picasso;
import com.travijuu.numberpicker.library.NumberPicker;

import java.util.ArrayList;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by azapata on 11/19/17.
 */

public class BuyDialogFragment2 extends DialogFragment implements BuyDialogFragmentMVP.View {
    private Product product;
    private BuyProductsAdapter adapter;
    private boolean reanOnly;
    private ArrayList<Object> people;


    @BindView(R.id.image_product_dialog)
    ImageView imageProduct;
    @BindView(R.id.text_name_item_dialog)
    TextView nameItem;
    @BindView(R.id.text_description_item_dialog)
    TextView descriptionItem;
    @BindView(R.id.button_buy_dialog)
    Button buttonBuy;
    @BindView(R.id.list_item_extras)
    ListView listView;
    @Inject
    BuyDialogFragmentMVP.Presenter presenter;
    private NumberPicker numberPicker;


    public BuyDialogFragment2() {
        // Required empty public constructor
    }

    public static BuyDialogFragment2 newInstance(Product product, boolean b) {
        BuyDialogFragment2 fragment = new BuyDialogFragment2();
        Bundle args = new Bundle();
        args.putParcelable(Constants.product, product);
        args.putBoolean(Constants.read, b);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getActivity().getApplication()).getComponent().inject(this);
        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_DeviceDefault_Dialog_Alert);
        if (getArguments() != null) {
            product = getArguments().getParcelable(Constants.product);
            reanOnly = getArguments().getBoolean(Constants.read);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.dialog_product_add, container, false);
        numberPicker = view.findViewById(R.id.number_picker);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Picasso.with(getContext()).load(product.image_url).into(imageProduct);
        people = new ArrayList<>();
        nameItem.setText(product.name);
        descriptionItem.setText(product.description);
        if (reanOnly) {
            numberPicker.setValue(product.qty);
            buttonBuy.setText(String.format(Locale.getDefault(), "ELIMINAR %.2f", product.price));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                buttonBuy.setBackgroundTintList(ContextCompat.getColorStateList(getContext(), R.color.colorRed));
            }
            numberPicker.setEnabled(false);
        } else {
            buttonBuy.setText(String.format(Locale.getDefault(), "COMPRAR %.2f", product.price));
        }

        if (product.options != null) {
            for (Option option : product.options) {
                people.add(option);
                if (option.option_details != null)
                    people.addAll(option.option_details);
            }
        }


        adapter = new BuyProductsAdapter(getContext(), people, this);

        listView.setAdapter(adapter);
        //setListViewHeightBasedOnChildren(listView);
        setSizeListView(people.size());
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.setView(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        //presenter.rxUnsubscribe();
    }

    @OnClick(R.id.button_buy_dialog)
    public void buy() {
        if (!reanOnly) validateData();
        else {
            String token = Prefs.getString(Constants.token, "");
            DeleteProduct deleteProduct = new DeleteProduct();
            deleteProduct.quantity = product.qty;
            deleteProduct.order_detail_id = product.order_detail_id;
            presenter.deleteProduct(deleteProduct, token);
        }
    }

    private void setSizeListView(int size) {
        ListAdapter listadp = listView.getAdapter();
        if (listadp != null) {
            int totalHeight = 0;
            for (int i = 0; i < size; i++) {
                View listItem = listadp.getView(i, null, listView);
                listItem.measure(0, 0);
                totalHeight += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight + (listView.getDividerHeight() * (listadp.getCount() - 1));
            listView.setLayoutParams(params);
            listView.requestLayout();
        }
    }

    private void validateData() {
        String token = Prefs.getString(Constants.token, "");
        if (token.isEmpty()) {
            AlertDialog dialog = createSimpleDialog();
            dialog.show();
            return;
        }

        ArrayList<Object> data = adapter.getPersonArray();
        ArrayList<Option> options = new ArrayList<>();

        for (Object o : data) {
            if (o instanceof Option) {
                boolean multichoice = ((Option) o).multichoice;
                boolean required = ((Option) o).required;
                int items = 0;

                for (OptionDetail optionDetail : ((Option) o).option_details) {
                    if (optionDetail.selected) {
                        items++;
                        options.add((Option) o);
                    }
                }

                if (items > 1) {
                    if (!multichoice) {
                        Toast.makeText(getContext(), "Solo puede escoger una opcion", Toast.LENGTH_LONG).show();
                        return;
                    }
                }

                if (items == 0 && required) {
                    Toast.makeText(getContext(), "Debe escoger almenos una opcion", Toast.LENGTH_LONG).show();
                    return;
                }
            }
        }
        product.options = options;
        product.qty = numberPicker.getValue();
        presenter.addProduct(product, "");
    }

    public void updateButton(Double price, boolean isChecked) {
        if (!reanOnly) {
            if (isChecked)
                product.price += price;
            else
                product.price -= price;

            buttonBuy.setText(String.format(Locale.getDefault(), "COMPRAR %.2f", product.price));
        }
    }

    @Override
    public void addedProduct() {
        Toast.makeText(getContext(), "Producto agregado al carrito de compras", Toast.LENGTH_LONG).show();
        this.dismiss();
    }

    @Override
    public void deletedProduct() {
        Toast.makeText(getContext(), "Producto eliminado del carrito de compras", Toast.LENGTH_LONG).show();
        this.dismiss();
    }

    @Override
    public void errorProducts(String s) {
        Toast.makeText(getContext(), s, Toast.LENGTH_LONG).show();
    }

    public AlertDialog createSimpleDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("Deli-baba")
                .setMessage("Necesita inicar sesion para comprar.")
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(getContext(), RegisterActivity.class);
                                startActivity(intent);
                                dialog.dismiss();
                                dismissDialogFrag();
                            }
                        });

        return builder.create();
    }

    private void dismissDialogFrag() {
        this.dismiss();
    }
}
