package com.delepues.buyer.store.dialog;

import com.delepues.buyer.http.apimodel.DeleteProduct;
import com.delepues.buyer.http.apimodel.OrderShopping;
import com.delepues.buyer.http.apimodel.Product;
import com.delepues.buyer.http.apimodel.ResponseCart;
import com.delepues.buyer.http.apimodel.SectionDataModel;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public interface Repository {
    boolean addProduct(Product product);
    boolean deleteProduct(String product);
    Observable<ArrayList<OrderShopping>> getResultsFromNetwork(ArrayList<Product> products,String buyerId);
    Observable<ResponseCart> getResultsFromNetwork(DeleteProduct product, String buyerId);
}
