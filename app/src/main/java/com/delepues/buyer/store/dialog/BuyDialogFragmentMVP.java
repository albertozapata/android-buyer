package com.delepues.buyer.store.dialog;

import com.delepues.buyer.http.apimodel.DeleteProduct;
import com.delepues.buyer.http.apimodel.OrderShopping;
import com.delepues.buyer.http.apimodel.Product;
import com.delepues.buyer.http.apimodel.ResponseCart;
import com.delepues.buyer.http.apimodel.SectionDataModel;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public interface BuyDialogFragmentMVP {
    interface View {
        void addedProduct();

        void deletedProduct();

        void errorProducts(String s);
    }

    interface Presenter {
        void addProduct(Product product, String buyerId);

        void deleteProduct(DeleteProduct product, String buyerId);

        void rxUnsubscribe();

        void setView(BuyDialogFragmentMVP.View view);
    }

    interface Model {
        boolean addProductLocal(Product product);

        boolean deleteProductLocal(String product);

        Observable<ArrayList<OrderShopping>> addProduct(Product product, String buyerId);

        Observable<ResponseCart> deleteProduct(DeleteProduct product, String buyerId);
    }
}
