package com.delepues.buyer.store;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.delepues.buyer.MainActivity;
import com.delepues.buyer.R;
import com.delepues.buyer.http.apimodel.Product;
import com.delepues.buyer.http.apimodel.SectionDataModel;

import java.util.ArrayList;

/**
 * Created by azapata on 10/22/17.
 */

public class SectionProductsAdapter extends RecyclerView.Adapter<SectionProductsAdapter.ItemRowHolder> {

    private ArrayList<SectionDataModel> dataList;
    private Context mContext;

    public SectionProductsAdapter(Context context, ArrayList<SectionDataModel> dataList) {
        this.dataList = dataList;
        this.mContext = context;
    }

    @Override
    public ItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.header_product_category, null);
        ItemRowHolder mh = new ItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(ItemRowHolder itemRowHolder, int i) {
        final SectionDataModel sectionDataModel = dataList.get(i);
        final String sectionName = sectionDataModel.category_name;
        ArrayList<Product> singleSectionItems = sectionDataModel.products;
        ProductsAdapter itemListDataAdapter;
        if (singleSectionItems.size() > 4) {
            itemRowHolder.plus.setVisibility(View.VISIBLE);
            itemListDataAdapter = new ProductsAdapter(mContext, new ArrayList<>(singleSectionItems.subList(0, 3)));
        } else {
            itemRowHolder.plus.setVisibility(View.GONE);
            itemListDataAdapter = new ProductsAdapter(mContext, singleSectionItems);
        }
        itemRowHolder.itemTitle.setText(sectionName);
        itemRowHolder.recycler_view_list.setHasFixedSize(true);
        itemRowHolder.recycler_view_list.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        itemRowHolder.recycler_view_list.setAdapter(itemListDataAdapter);
        itemRowHolder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)mContext).changeFragment("Products",sectionDataModel.products);
            }
        });
//
//        itemRowHolder.btnMore.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(v.getContext(), "click event on more, " + sectionName, Toast.LENGTH_SHORT).show();
//            }
//        });


       /* Glide.with(mContext)
                .load(feedItem.getImageURL())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .error(R.drawable.bg)
                .into(feedListRowHolder.thumbView);*/
    }

    @Override
    public int getItemCount() {
        return (null != dataList ? dataList.size() : 0);
    }

    public class ItemRowHolder extends RecyclerView.ViewHolder {
        protected TextView itemTitle, plus;
        protected RecyclerView recycler_view_list;

        public ItemRowHolder(View view) {
            super(view);
            this.itemTitle = view.findViewById(R.id.itemTitle);
            this.plus = view.findViewById(R.id.item_plus);
            this.recycler_view_list = view.findViewById(R.id.recycler_view_list);
        }

    }

}