package com.delepues.buyer.store.dialog;

import com.delepues.buyer.http.apimodel.DeleteProduct;
import com.delepues.buyer.http.apimodel.OrderShopping;
import com.delepues.buyer.http.apimodel.Product;
import com.delepues.buyer.http.apimodel.ResponseCart;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public class BuyDialogModel implements BuyDialogFragmentMVP.Model {
    private Repository repository;

    public BuyDialogModel(Repository repository) {
        this.repository = repository;
    }

    @Override
    public Observable<ArrayList<OrderShopping>> addProduct(Product product, String buyerId) {
        ArrayList<Product> products = new ArrayList<>();
        products.add(product);
        return repository.getResultsFromNetwork(products, buyerId);
    }

    @Override
    public Observable<ResponseCart> deleteProduct(DeleteProduct product, String buyerId) {
        return repository.getResultsFromNetwork(product, buyerId);
    }

    @Override
    public boolean addProductLocal(Product product) {
        return repository.addProduct(product);
    }

    @Override
    public boolean deleteProductLocal(String product) {
        return repository.deleteProduct(product);
    }
}
