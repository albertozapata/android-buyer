package com.delepues.buyer.store;

import com.delepues.buyer.http.BuyerApiService;
import com.delepues.buyer.http.apimodel.Option;
import com.delepues.buyer.http.apimodel.OptionDetail;
import com.delepues.buyer.http.apimodel.Product;
import com.delepues.buyer.http.apimodel.SectionDataModel;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmList;
import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public class StoreRepository implements Repository {
    BuyerApiService buyerApiService;

    public StoreRepository(BuyerApiService buyerApiService) {
        this.buyerApiService = buyerApiService;
    }

    @Override
    public Observable<ArrayList<SectionDataModel>> getResultsFromNetwork(String branchId) {
        return buyerApiService.branchProducts("deb03f8e-8339-4410-89a2-671c2b25edce",branchId);
    }

    @Override
    public Observable<ArrayList<SectionDataModel>> getResultsFromLocal(String branchId) {
        ArrayList<SectionDataModel> sectionDataModels = new ArrayList<>();

        SectionDataModel sectionDataModel = new SectionDataModel();
        ArrayList<Product> products = new ArrayList<>();

        sectionDataModel.category_name = "Carnes";
        sectionDataModel.category_id = "1";

        products.add(new Product("1", "Pierna con muslo de pollo congelado U.S.A", 27.50, "http://www.lacolonia.com.ni/slc_admin/ARCHIVOS/GALERIA/6030126.jpg", "Pierna con muslo de pollo congelado U.S.A", getOptions()));
        products.add(new Product("2", "Pechuga de pollo fresca RICO / TIP-TOP", 56.25, "http://www.lacolonia.com.ni/slc_admin/ARCHIVOS/GALERIA/1700015.jpg", "Pechuga de pollo fresca RICO / TIP-TOP", getOptions()));
        products.add(new Product("3", "Carne molida súper especial LIBRA", 59.96, "http://www.lacolonia.com.ni/slc_admin/ARCHIVOS/GALERIA/6080220.jpg", "Carne molida súper especial LIBRA", getOptions()));
        products.add(new Product("4", "Punta de cacho LIBRA", 81.60, "http://www.lacolonia.com.ni/slc_admin/ARCHIVOS/GALERIA/6080103.jpg", "Punta de cacho LIBRA", null));
        products.add(new Product("5", "Camaron con cascara 36-40 LIBRA", 319.20, "http://www.lacolonia.com.ni/slc_admin/ARCHIVOS/GALERIA/1720026.jpg", "Camaron con cascara 36-40 LIBRA", null));
        sectionDataModel.products = products;
        sectionDataModels.add(sectionDataModel);

        products = new ArrayList<>();
        sectionDataModel = new SectionDataModel();
        sectionDataModel.category_name = "Lacteos, Embutidos y Otros";
        sectionDataModel.category_id = "2";
        products.add(new Product("1", "Pollo rostizado entero", 167.25, "http://www.lacolonia.com.ni/slc_admin/ARCHIVOS/GALERIA/1610299.jpg", "Pollo rostizado entero + gratis pepsi 2.25 lts ECONOMAX", getOptions()));
        products.add(new Product("2", "Yogurt bebible con azúcar 235 grs 4pk YOPLAIT", 76.52, "http://www.lacolonia.com.ni/slc_admin/ARCHIVOS/GALERIA/1510936.jpg", "Yogurt bebible con azúcar 235 grs 4pk YOPLAIT", null));
        products.add(new Product("3", "Peperoni libra ZAR", 145.88, "http://www.lacolonia.com.ni/slc_admin/ARCHIVOS/GALERIA/1541633.jpg", "Peperoni libra ZAR", getOptions()));
        products.add(new Product("4", "Queso original crumbled cup 4 oz MONTCHEVRE", 119.81, "http://www.lacolonia.com.ni/slc_admin/ARCHIVOS/GALERIA/1351736.jpg", "Queso original crumbled cup 4 oz MONTCHEVRE", null));
        products.add(new Product("5", "Baguette rustico BUONPAN", 40.25, "http://www.lacolonia.com.ni/slc_admin/ARCHIVOS/GALERIA/4070377.JPG", "Camaron con cascara 36-40 LIBRA", getOptions()));
        sectionDataModel.products = products;
        sectionDataModels.add(sectionDataModel);

        products = new ArrayList<>();
        sectionDataModel = new SectionDataModel();
        sectionDataModel.category_name = "Frutas y Vegetales";
        sectionDataModel.category_id = "3";
        products.add(new Product("1", "Lechuga escarola verde LIBRA", 11.63, "http://www.lacolonia.com.ni/slc_admin/ARCHIVOS/GALERIA/5010304.jpg", "Lechuga escarola verde LIBRA", getOptions()));
        products.add(new Product("2", "Yuca parafinada LIBRA", 5.63, "http://www.lacolonia.com.ni/slc_admin/ARCHIVOS/GALERIA/5010055.jpg", "Yuca parafinada LIBRA", getOptions()));
        products.add(new Product("3", "Manzana red delic # 100 bol 6's BOLSA", 114.38, "http://www.lacolonia.com.ni/slc_admin/ARCHIVOS/GALERIA/1561074.jpg", "Manzana red delic # 100 bol 6's BOLSA", null));
        products.add(new Product("4", "Chile dulce malla 4`s MALLA", 25.03, "http://www.lacolonia.com.ni/slc_admin/ARCHIVOS/GALERIA/5010469.jpg", "Chile dulce malla 4`s MALLA", null));
        products.add(new Product("5", "Banano maduro premium UNIDAD", 2.25, "http://www.lacolonia.com.ni/slc_admin/ARCHIVOS/GALERIA/5040001.jpg", "Banano maduro premium UNIDAD", null));
        sectionDataModel.products = products;
        sectionDataModels.add(sectionDataModel);

        return Observable.just(sectionDataModels);
    }

    public ArrayList<Option> getOptions() {
        ArrayList<Option> options = new ArrayList<>();
        Option option = new Option();
        option.name = "Tipo de sabor:";
        option.required = true;
        option.option_id = "1";
        option.multichoice = false;
        ArrayList<OptionDetail> optionDetails = new ArrayList<>();
        OptionDetail optionDetail = new OptionDetail();
        optionDetail.option_detail_id = "1";
        optionDetail.name = "Gallopinto";
        optionDetail.price = 0.0;
        optionDetails.add(optionDetail);
        optionDetail = new OptionDetail();
        optionDetail.option_detail_id = "2";
        optionDetail.name = "Arroz Blanco";
        optionDetail.price = 0.0;
        optionDetails.add(optionDetail);
        optionDetail = new OptionDetail();
        optionDetail.option_detail_id = "3";
        optionDetail.name = "Arroz Amarillo";
        optionDetail.price = 0.0;
        optionDetails.add(optionDetail);
        option.option_details = optionDetails;
        options.add(option);

        option = new Option();
        option.name = "Extras de Bebidas:";
        option.required = false;
        option.option_id = "1";
        option.multichoice = true;
        optionDetails = new ArrayList<>();
        optionDetail = new OptionDetail();
        optionDetail.option_detail_id = "1";
        optionDetail.name = "Pepsi Lata";
        optionDetail.price = 15.0;
        optionDetails.add(optionDetail);
        optionDetail = new OptionDetail();
        optionDetail.option_detail_id = "2";
        optionDetail.name = "Pepsi Black Lata";
        optionDetail.price = 15.0;
        optionDetails.add(optionDetail);
        optionDetail = new OptionDetail();
        optionDetail.option_detail_id = "3";
        optionDetail.name = "Rojita Lata";
        optionDetail.price = 15.0;
        optionDetails.add(optionDetail);
        option.option_details = optionDetails;
        options.add(option);

        return options;
    }
}
