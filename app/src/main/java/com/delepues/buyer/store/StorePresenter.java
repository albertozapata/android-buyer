package com.delepues.buyer.store;

import com.delepues.buyer.http.apimodel.Category;
import com.delepues.buyer.http.apimodel.SectionDataModel;
import com.delepues.buyer.landing.LandingFragmentMVP;

import java.util.ArrayList;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by azapata on 10/12/17.
 */

public class StorePresenter implements StoreFragmentMVP.Presenter {
    private StoreFragmentMVP.View view;
    private Subscription subscription = null;
    private StoreFragmentMVP.Model model;
    private ArrayList<SectionDataModel> products = new ArrayList<>();

    public StorePresenter(StoreFragmentMVP.Model model) {
        this.model = model;
    }

    @Override
    public void getProducts(String branchId) {
        subscription = model.getProducts(branchId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ArrayList<SectionDataModel>>() {
                    @Override
                    public void onCompleted() {
                        if (!products.isEmpty())
                            view.showProducts(products);
                        else
                            view.errorEmpty();
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.errorProducts(e.getMessage());
                    }

                    @Override
                    public void onNext(ArrayList<SectionDataModel> sectionDataModels) {
                        products = sectionDataModels;
                    }
                });
    }

    @Override
    public void rxUnsubscribe() {
        if (subscription != null) {
            if (!subscription.isUnsubscribed()) {
                subscription.unsubscribe();
            }
        }
    }

    @Override
    public void setView(StoreFragmentMVP.View view) {
        this.view = view;
    }
}
