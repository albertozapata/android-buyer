package com.delepues.buyer.root;

import android.app.Application;
import android.content.ContextWrapper;

import com.crashlytics.android.Crashlytics;
import com.delepues.buyer.address.AddressModule;
import com.delepues.buyer.category.StoresModule;
import com.delepues.buyer.history.HistoryModule;
import com.delepues.buyer.http.ApiModule;
import com.delepues.buyer.landing.LandingModule;
import com.delepues.buyer.login.LoginModule;
import com.delepues.buyer.profile.ProfileModule;
import com.delepues.buyer.register.RegisterModule;
import com.delepues.buyer.shopping.ShoppingCartModule;
import com.delepues.buyer.store.StoreModule;
import com.delepues.buyer.store.dialog.BuyDialogModule;
import com.pixplicity.easyprefs.library.Prefs;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by azapata on 10/10/17.
 */

public class App extends Application {

    private ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("delepues.realm")
                .schemaVersion(1)
                .build();
        Realm.setDefaultConfiguration(config);

        new Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(getPackageName())
                .setUseDefaultSharedPreference(true)
                .build();

        component = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .apiModule(new ApiModule())
                .landingModule(new LandingModule())
                .loginModule(new LoginModule())
                .storesModule(new StoresModule())
                .storeModule(new StoreModule())
                .buyDialogModule(new BuyDialogModule())
                .shoppingCartModule(new ShoppingCartModule())
                .addressModule(new AddressModule())
                .registerModule(new RegisterModule())
                .historyModule(new HistoryModule())
                .profileModule(new ProfileModule())
                .build();
    }

    public ApplicationComponent getComponent() {
        return component;
    }
}
