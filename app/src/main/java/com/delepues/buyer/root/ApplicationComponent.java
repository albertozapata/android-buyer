package com.delepues.buyer.root;

import com.delepues.buyer.address.AddressFragment;
import com.delepues.buyer.address.AddressModule;
import com.delepues.buyer.category.StoresFragment;
import com.delepues.buyer.category.StoresModule;
import com.delepues.buyer.history.HistoryFragment;
import com.delepues.buyer.history.HistoryModule;
import com.delepues.buyer.http.ApiModule;
import com.delepues.buyer.landing.LandingFragment;
import com.delepues.buyer.landing.LandingModule;
import com.delepues.buyer.login.LoginFragment;
import com.delepues.buyer.login.LoginModule;
import com.delepues.buyer.profile.ProfileFragment;
import com.delepues.buyer.profile.ProfileModule;
import com.delepues.buyer.register.RegisterActivity;
import com.delepues.buyer.register.RegisterModule;
import com.delepues.buyer.shopping.ShoppingCartFragment;
import com.delepues.buyer.shopping.ShoppingCartModule;
import com.delepues.buyer.store.StoreFragment;
import com.delepues.buyer.store.StoreModule;
import com.delepues.buyer.store.dialog.BuyDialogFragment2;
import com.delepues.buyer.store.dialog.BuyDialogModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by azapata on 10/10/17.
 */

@Singleton
@Component(modules = {ApplicationModule.class, ApiModule.class, LandingModule.class, LoginModule.class, StoresModule.class, StoreModule.class, BuyDialogModule.class, ShoppingCartModule.class, AddressModule.class, RegisterModule.class, HistoryModule.class, ProfileModule.class})
public interface ApplicationComponent {

    void inject(LandingFragment target);

    void inject(LoginFragment target);

    void inject(StoresFragment target);

    void inject(StoreFragment target);

    void inject(BuyDialogFragment2 target);

    void inject(ShoppingCartFragment target);

    void inject(AddressFragment target);

    void inject(RegisterActivity target);

    void inject(HistoryFragment target);

    void inject(ProfileFragment target);

}
