package com.delepues.buyer.http.apimodel;

/**
 * Created by azapata on 11/24/17.
 */

public class BuyerResponse {
    public String user_profile_id;
    public String id;
    public String username;
    public String first_name;
    public String last_name;
    public String phone;
    public String email;
}