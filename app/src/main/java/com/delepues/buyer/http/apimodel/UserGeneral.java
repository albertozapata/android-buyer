package com.delepues.buyer.http.apimodel;

/**
 * Created by azapata on 10/31/17.
 */

public class UserGeneral {
    public String user_id;
    public String user_state_id;
    public String username;
    public String email;
    public String first_name;
    public String last_name;
    public String description;

    public UserGeneral(String user_id, String user_state_id, String username, String email, String first_name, String last_name, String description) {
        this.user_id = user_id;
        this.user_state_id = user_state_id;
        this.username = username;
        this.email = email;
        this.first_name = first_name;
        this.last_name = last_name;
        this.description = description;
    }
}