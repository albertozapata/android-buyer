package com.delepues.buyer.http.apimodel;

/**
 * Created by azapata on 10/31/17.
 */

public class Login {
    public String login;
    public String password;

    public Login(String login, String password) {
        this.login = login;
        this.password = password;
    }
}
