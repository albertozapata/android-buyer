package com.delepues.buyer.http.apimodel;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by azapata on 10/31/17.
 */

public class Address implements Parcelable {
    public String location = "undefined";
    public double longitude = 0;
    public double latitude = 0;
    public String city_id;

    public String address_id;
    public String address;

    public Address() {
    }

    public Address(String location, double longitude, double latitude) {
        this.location = location;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    protected Address(Parcel in) {
        location = in.readString();
        longitude = in.readDouble();
        latitude = in.readDouble();
        city_id = in.readString();
        address_id = in.readString();
        address = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(location);
        dest.writeDouble(longitude);
        dest.writeDouble(latitude);
        dest.writeString(city_id);
        dest.writeString(address_id);
        dest.writeString(address);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Address> CREATOR = new Parcelable.Creator<Address>() {
        @Override
        public Address createFromParcel(Parcel in) {
            return new Address(in);
        }

        @Override
        public Address[] newArray(int size) {
            return new Address[size];
        }
    };
}
