package com.delepues.buyer.http.apimodel;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by azapata on 12/3/17.
 */

public class OrderDetail implements Parcelable {
    public String name;
    public double price;
    public double quantity;
    public String image_url;
    public ArrayList<OrderOption> options;

    public OrderDetail() {
    }

    protected OrderDetail(Parcel in) {
        name = in.readString();
        price = in.readDouble();
        quantity = in.readDouble();
        image_url = in.readString();
        if (in.readByte() == 0x01) {
            options = new ArrayList<OrderOption>();
            in.readList(options, OrderOption.class.getClassLoader());
        } else {
            options = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeDouble(price);
        dest.writeDouble(quantity);
        dest.writeString(image_url);
        if (options == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(options);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<OrderDetail> CREATOR = new Parcelable.Creator<OrderDetail>() {
        @Override
        public OrderDetail createFromParcel(Parcel in) {
            return new OrderDetail(in);
        }

        @Override
        public OrderDetail[] newArray(int size) {
            return new OrderDetail[size];
        }
    };
}