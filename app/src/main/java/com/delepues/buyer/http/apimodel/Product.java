package com.delepues.buyer.http.apimodel;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by azapata on 10/22/17.
 */

public class Product implements Parcelable {

    public String store_branch_product_id;
    public String name;
    public double price;
    public String image_url;
    public String description;
    public String order_detail_id;
    public int qty = 0;
    public ArrayList<Option> options;
    public String order_id;

    public Product() {
    }

    public Product(String id, String name, double price, String image_url, String description, ArrayList<Option> options) {
        this.store_branch_product_id = id;
        this.name = name;
        this.price = price;
        this.image_url = image_url;
        this.description = description;
        this.options = options;
    }

    protected Product(Parcel in) {
        store_branch_product_id = in.readString();
        name = in.readString();
        price = in.readDouble();
        image_url = in.readString();
        description = in.readString();
        order_detail_id = in.readString();
        qty = in.readInt();
        if (in.readByte() == 0x01) {
            options = new ArrayList<Option>();
            in.readList(options, Option.class.getClassLoader());
        } else {
            options = null;
        }
        order_id = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(store_branch_product_id);
        dest.writeString(name);
        dest.writeDouble(price);
        dest.writeString(image_url);
        dest.writeString(description);
        dest.writeString(order_detail_id);
        dest.writeInt(qty);
        if (options == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(options);
        }
        dest.writeString(order_id);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Product> CREATOR = new Parcelable.Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };
}