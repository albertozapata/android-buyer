package com.delepues.buyer.http.apimodel;

/**
 * Created by azapata on 11/29/17.
 */

public class CartItem {
    public String store_branch_product_id;
    public int quantity;
    public String note;
    public OptionDetail options;
}
