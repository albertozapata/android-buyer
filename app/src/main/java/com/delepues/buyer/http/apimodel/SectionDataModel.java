package com.delepues.buyer.http.apimodel;

import java.util.ArrayList;

/**
 * Created by azapata on 10/22/17.
 */

public class SectionDataModel {
    public String category_id;
    public String category_name;
    public ArrayList<Product> products;
}
