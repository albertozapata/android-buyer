package com.delepues.buyer.http.apimodel;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by azapata on 10/12/17.
 */

public class Category implements Parcelable {
    public String id;
    public String parent_id;
    public String country_id;
    public String name;
    public String image_url;
    public String image_file_name;
    public String image_details;

    public Category(String id, String name, String image_url) {
        this.id = id;
        this.name = name;
        this.image_url = image_url;
    }

    protected Category(Parcel in) {
        id = in.readString();
        parent_id = in.readString();
        country_id = in.readString();
        name = in.readString();
        image_url = in.readString();
        image_file_name = in.readString();
        image_details = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(parent_id);
        dest.writeString(country_id);
        dest.writeString(name);
        dest.writeString(image_url);
        dest.writeString(image_file_name);
        dest.writeString(image_details);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Category> CREATOR = new Parcelable.Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };
}