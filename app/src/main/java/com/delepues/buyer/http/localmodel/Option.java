package com.delepues.buyer.http.localmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.delepues.buyer.http.apimodel.OptionDetail;

import java.util.ArrayList;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by azapata on 11/15/17.
 */

public class Option extends RealmObject {
    public String option_id;
    public String name;
    public boolean required;
    public boolean multichoice;
    public RealmList<com.delepues.buyer.http.localmodel.OptionDetail> option_details;
}
