package com.delepues.buyer.http.apimodel;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by azapata on 11/14/17.
 */

public class Branch implements Parcelable {
    public String store_id;
    public String store_name;
    public String store_category_id;
    public String store_branch_id;
    public String store_branch_name;
    public String image_url;
    public String banner_url;
    public String open_time;
    public String close_time;
    public String description;
    public String address;
    public double latitude;
    public double longitude;
    public String city_name;
    public String distance;

    public Branch(String store_id, String store_name, String store_branch_name, String image_url, String banner_url, String open_time, String close_time, String description, String distance,String store_branch_id) {
        this.store_id = store_id;
        this.store_name = store_name;
        this.store_branch_name = store_branch_name;
        this.image_url = image_url;
        this.banner_url = banner_url;
        this.open_time = open_time;
        this.close_time = close_time;
        this.description = description;
        this.distance = distance;
        this.store_branch_id = store_branch_id;
    }

    protected Branch(Parcel in) {
        store_id = in.readString();
        store_name = in.readString();
        store_category_id = in.readString();
        store_branch_id = in.readString();
        store_branch_name = in.readString();
        image_url = in.readString();
        banner_url = in.readString();
        open_time = in.readString();
        close_time = in.readString();
        description = in.readString();
        address = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
        city_name = in.readString();
        distance = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(store_id);
        dest.writeString(store_name);
        dest.writeString(store_category_id);
        dest.writeString(store_branch_id);
        dest.writeString(store_branch_name);
        dest.writeString(image_url);
        dest.writeString(banner_url);
        dest.writeString(open_time);
        dest.writeString(close_time);
        dest.writeString(description);
        dest.writeString(address);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeString(city_name);
        dest.writeString(distance);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Branch> CREATOR = new Parcelable.Creator<Branch>() {
        @Override
        public Branch createFromParcel(Parcel in) {
            return new Branch(in);
        }

        @Override
        public Branch[] newArray(int size) {
            return new Branch[size];
        }
    };
}
