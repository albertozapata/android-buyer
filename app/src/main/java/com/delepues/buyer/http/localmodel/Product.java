package com.delepues.buyer.http.localmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.delepues.buyer.http.apimodel.Option;

import java.util.ArrayList;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by azapata on 10/22/17.
 */

public class Product extends RealmObject {
    public String store_branch_product_id;
    public String name;
    public int qty = 0;
    public double price;
    public String image_url;
    public String description;
    public String order_detail_id;
    public String order_id;
    public RealmList<com.delepues.buyer.http.localmodel.Option> options;
}