package com.delepues.buyer.http.apimodel;

/**
 * Created by azapata on 10/31/17.
 */

public class LoginResponse {
    public User user;
    public String token;
    public boolean success;

    public LoginResponse(User user, String token, boolean success) {
        this.user = user;
        this.token = token;
        this.success = success;
    }
}
