package com.delepues.buyer.http.apimodel;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by azapata on 12/18/17.
 */

public class Profile implements Parcelable{
    public String user_profile_id;
    public String user_id;
    public String buyer_id;
    public String user_state_id;
    public String username;
    public String first_name;
    public String last_name;
    public String phone;
    public String email;
    public ArrayList<Address> addresses;

    public Profile() {
    }

    protected Profile(Parcel in) {
        user_profile_id = in.readString();
        user_id = in.readString();
        buyer_id = in.readString();
        user_state_id = in.readString();
        username = in.readString();
        first_name = in.readString();
        last_name = in.readString();
        phone = in.readString();
        email = in.readString();
        if (in.readByte() == 0x01) {
            addresses = new ArrayList<Address>();
            in.readList(addresses, Address.class.getClassLoader());
        } else {
            addresses = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(user_profile_id);
        dest.writeString(user_id);
        dest.writeString(buyer_id);
        dest.writeString(user_state_id);
        dest.writeString(username);
        dest.writeString(first_name);
        dest.writeString(last_name);
        dest.writeString(phone);
        dest.writeString(email);
        if (addresses == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(addresses);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Profile> CREATOR = new Parcelable.Creator<Profile>() {
        @Override
        public Profile createFromParcel(Parcel in) {
            return new Profile(in);
        }

        @Override
        public Profile[] newArray(int size) {
            return new Profile[size];
        }
    };
}
