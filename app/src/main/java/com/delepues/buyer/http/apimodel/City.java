package com.delepues.buyer.http.apimodel;

/**
 * Created by azapata on 11/20/17.
 */

public class City {
    public String id;
    public String country_id;
    public String name;

    public City(String id, String country_id, String name) {
        this.id = id;
        this.country_id = country_id;
        this.name = name;
    }
}
