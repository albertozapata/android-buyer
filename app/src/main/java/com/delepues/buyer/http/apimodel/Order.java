package com.delepues.buyer.http.apimodel;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by azapata on 12/3/17.
 */

public class Order implements Parcelable {
    public String id;
    public String buyer_id;
    public String store_branch_id;
    public String driver_id;
    public String order_state_id;
    public String pick_up_time;
    public String delivery_time;
    public String branch_name;
    public String branch_image_url;
    public String state_name;
    public boolean in_progress;
    public ArrayList<OrderDetail> details;

    public Order() {
    }

    protected Order(Parcel in) {
        id = in.readString();
        buyer_id = in.readString();
        store_branch_id = in.readString();
        driver_id = in.readString();
        order_state_id = in.readString();
        pick_up_time = in.readString();
        delivery_time = in.readString();
        branch_name = in.readString();
        branch_image_url = in.readString();
        state_name = in.readString();
        in_progress = in.readByte() != 0x00;
        if (in.readByte() == 0x01) {
            details = new ArrayList<OrderDetail>();
            in.readList(details, OrderDetail.class.getClassLoader());
        } else {
            details = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(buyer_id);
        dest.writeString(store_branch_id);
        dest.writeString(driver_id);
        dest.writeString(order_state_id);
        dest.writeString(pick_up_time);
        dest.writeString(delivery_time);
        dest.writeString(branch_name);
        dest.writeString(branch_image_url);
        dest.writeString(state_name);
        dest.writeByte((byte) (in_progress ? 0x01 : 0x00));
        if (details == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(details);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Order> CREATOR = new Parcelable.Creator<Order>() {
        @Override
        public Order createFromParcel(Parcel in) {
            return new Order(in);
        }

        @Override
        public Order[] newArray(int size) {
            return new Order[size];
        }
    };
}
