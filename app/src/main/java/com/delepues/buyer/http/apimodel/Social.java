package com.delepues.buyer.http.apimodel;

/**
 * Created by azapata on 10/31/17.
 */

public class Social {
    public Address address;
    public String phone = "undefined";
    public String network = "facebook";
    public String token;

    public Social(Address address, String phone, String network, String token) {
        this.address = address;
        this.phone = phone;
        this.network = network;
        this.token = token;
    }
}
