package com.delepues.buyer.http.apimodel;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by azapata on 12/3/17.
 */

public class OrderOption implements Parcelable {
    public String name;
    public double price;
    public String option_detail_id;

    public OrderOption() {
    }

    protected OrderOption(Parcel in) {
        name = in.readString();
        price = in.readDouble();
        option_detail_id = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeDouble(price);
        dest.writeString(option_detail_id);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<OrderOption> CREATOR = new Parcelable.Creator<OrderOption>() {
        @Override
        public OrderOption createFromParcel(Parcel in) {
            return new OrderOption(in);
        }

        @Override
        public OrderOption[] newArray(int size) {
            return new OrderOption[size];
        }
    };
}
