package com.delepues.buyer.http.apimodel;

/**
 * Created by azapata on 11/24/17.
 */

public class Buyer {
    public String username;
    public String password;
    public String first_name;
    public String last_name;
    public String image_url;
    public String image_file_name;
    public String phone;
    public String email;
    public Address address;
}