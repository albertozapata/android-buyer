package com.delepues.buyer.http;

import com.delepues.buyer.http.apimodel.Address;
import com.delepues.buyer.http.apimodel.Branch;
import com.delepues.buyer.http.apimodel.Buyer;
import com.delepues.buyer.http.apimodel.BuyerResponse;
import com.delepues.buyer.http.apimodel.CartItem;
import com.delepues.buyer.http.apimodel.Category;
import com.delepues.buyer.http.apimodel.City;
import com.delepues.buyer.http.apimodel.DeleteProduct;
import com.delepues.buyer.http.apimodel.Login;
import com.delepues.buyer.http.apimodel.LoginResponse;
import com.delepues.buyer.http.apimodel.LoginSocial;
import com.delepues.buyer.http.apimodel.Order;
import com.delepues.buyer.http.apimodel.OrderShopping;
import com.delepues.buyer.http.apimodel.Product;
import com.delepues.buyer.http.apimodel.Profile;
import com.delepues.buyer.http.apimodel.ResponseCart;
import com.delepues.buyer.http.apimodel.SectionDataModel;

import java.util.ArrayList;

import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by azapata on 10/10/17.
 */

public interface BuyerApiService {


    @GET("countries/{country}/store-categories")
    Observable<ArrayList<Category>> storeCategories(@Header("dpx-access-token") String token, @Path("country") String country);

    @GET("store-categories/{categoryId}/store-branches")
    Observable<ArrayList<Branch>> storeBranches(@Header("dpx-access-token") String token,@Path("categoryId") String categoryId, @Query("latitude") Double latitude, @Query("longitude") Double longitude);

    @GET("store-branches/{branchId}/products")
    Observable<ArrayList<SectionDataModel>> branchProducts(@Header("dpx-access-token") String token,@Path("branchId") String branchId);

    @POST("auth")
    Observable<Response<LoginResponse>> loginUser(@Header("dpx-access-token") String token,@Body Login login);

    @POST("auth")
    Observable<Response<LoginResponse>> loginSocial(@Header("dpx-access-token") String token,@Body LoginSocial login);

    @GET("countries/{country}/cities")
    Observable<ArrayList<City>> cities(@Header("dpx-access-token") String token,@Path("country") String country);

    @POST("buyers")
    Observable<BuyerResponse> register(@Header("dpx-access-token") String token,@Body Buyer buyer);

    @POST("buyers/{buyerId}/carts")
    Observable<ArrayList<OrderShopping>> addProduct(@Body ArrayList<Product> products, @Path("buyerId") String buyerId);

    @POST("buyers/{buyerId}/carts")
    Observable<ResponseCart> deleteProduct(@Body DeleteProduct product, @Path("buyerId") String buyerId);

//    @POST("buyers")
//    Observable<BuyerResponse> shoppingCart(@Body ArrayList<CartItem> items);

    @GET("buyers/{buyerId}/carts ")
    Observable<ArrayList<City>> shoppingCart(@Path("buyerId") String buyerId);

    @GET("buyers/{buyerId}/orders?status=all")
    Observable<ArrayList<Order>> orders(@Path("buyerId") String buyerId);

    @GET("buyers/{buyerId}")
    Observable<Profile> profile(@Path("buyerId") String buyerId);

    @POST("{buyerId}/addresses")
    Observable<ResponseCart> addAddress(@Body Address address, @Path("buyerId") String buyerId);

}
