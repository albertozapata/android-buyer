package com.delepues.buyer.http.apimodel;

/**
 * Created by azapata on 11/15/17.
 */

public class OptionDetail {
    public String option_detail_id;
    public String name;
    public double price;
    public boolean selected = false;
}
