package com.delepues.buyer.http.apimodel;

/**
 * Created by azapata on 12/14/17.
 */

public class DeleteProduct {
    public String order_detail_id;
    public double quantity;
}
