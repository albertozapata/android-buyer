package com.delepues.buyer.http.localmodel;

import io.realm.RealmObject;

/**
 * Created by azapata on 11/15/17.
 */

public class OptionDetail extends RealmObject{
    public String option_detail_id;
    public String name;
    public double price;
    public boolean selected = false;
}
