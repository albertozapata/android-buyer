package com.delepues.buyer.http.apimodel;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by azapata on 11/15/17.
 */

public class Option implements Parcelable {
    public String option_id;
    public String name;
    public boolean required;
    public boolean multichoice;
    public ArrayList<OptionDetail> option_details;

    protected Option(Parcel in) {
        option_id = in.readString();
        name = in.readString();
        required = in.readByte() != 0x00;
        multichoice = in.readByte() != 0x00;
        if (in.readByte() == 0x01) {
            option_details = new ArrayList<OptionDetail>();
            in.readList(option_details, OptionDetail.class.getClassLoader());
        } else {
            option_details = null;
        }
    }

    public Option() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(option_id);
        dest.writeString(name);
        dest.writeByte((byte) (required ? 0x01 : 0x00));
        dest.writeByte((byte) (multichoice ? 0x01 : 0x00));
        if (option_details == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(option_details);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Option> CREATOR = new Parcelable.Creator<Option>() {
        @Override
        public Option createFromParcel(Parcel in) {
            return new Option(in);
        }

        @Override
        public Option[] newArray(int size) {
            return new Option[size];
        }
    };
}
