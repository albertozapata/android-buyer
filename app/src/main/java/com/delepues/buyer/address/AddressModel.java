package com.delepues.buyer.address;


import com.delepues.buyer.http.apimodel.City;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public class AddressModel implements AddressFragmentMVP.Model {
    private Repository repository;

    public AddressModel(Repository repository) {
        this.repository = repository;
    }


    @Override
    public Observable<ArrayList<City>> getCities(String country) {
        return repository.getResultsFromLocal(country);
    }
}
