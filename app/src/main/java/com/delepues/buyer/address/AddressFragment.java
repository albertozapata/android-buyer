package com.delepues.buyer.address;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Toast;

import com.delepues.buyer.R;
import com.delepues.buyer.http.apimodel.City;
import com.delepues.buyer.root.App;
import com.delepues.buyer.utils.Constants;
import com.satsuware.usefulviews.LabelledSpinner;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddressFragment extends Fragment implements AddressFragmentMVP.View {

    private String country;

    LabelledSpinner yourSpinner;
    @Inject
    AddressFragmentMVP.Presenter presenter;
    @BindView(R.id.edit_address)
    TextInputEditText address;
    String city = "";

    @BindView(R.id.button_address_next)
    Button nextFragment;
    public ArrayList<City> cities;

    public AddressFragment() {
        // Required empty public constructor
    }

    public static AddressFragment newInstance(String country) {
        AddressFragment fragment = new AddressFragment();
        Bundle args = new Bundle();
        args.putString(Constants.codeCountry, country);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getActivity().getApplication()).getComponent().inject(this);
        if (getArguments() != null) {
            country = getArguments().getString(Constants.codeCountry);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_address, container, false);
        yourSpinner = view.findViewById(R.id.your_labelled_spinner);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //yourSpinner.setItemsArray(R.array.system);
        presenter.getCities(country);
        yourSpinner.setOnItemChosenListener(new LabelledSpinner.OnItemChosenListener() {
            @Override
            public void onItemChosen(View labelledSpinner, AdapterView<?> adapterView, View itemView, int position, long id) {
                city = cities.get(position).id;
            }

            @Override
            public void onNothingChosen(View labelledSpinner, AdapterView<?> adapterView) {

            }
        });
    }

    @OnClick(R.id.button_address_next)
    public void next() {
        String text = address.getText().toString();
        if (!text.isEmpty())
            ((AddressActivity) getActivity()).changeFragment(text, city);
        else
            Toast.makeText(getContext(), "Por favor ingrese una direccion valida", Toast.LENGTH_LONG).show();
    }

    @Override
    public void showCities(ArrayList<City> cities) {
        this.cities = cities;
        List<String> cities1 = new ArrayList<>();
        for (City city : cities) {
            cities1.add(city.name);
        }
        yourSpinner.setItemsArray(cities1);
    }

    @Override
    public void errorCities(String s) {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void errorEmpty() {

    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.setView(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.rxUnsubscribe();
    }
}
