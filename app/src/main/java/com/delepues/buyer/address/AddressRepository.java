package com.delepues.buyer.address;

import com.delepues.buyer.http.BuyerApiService;
import com.delepues.buyer.http.apimodel.City;
import com.delepues.buyer.http.apimodel.SectionDataModel;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public class AddressRepository implements Repository {
    BuyerApiService buyerApiService;

    public AddressRepository(BuyerApiService buyerApiService) {
        this.buyerApiService = buyerApiService;
    }


    @Override
    public Observable<ArrayList<City>> getResultsFromNetwork(String country) {
        return buyerApiService.cities("deb03f8e-8339-4410-89a2-671c2b25edce",country);
    }

    @Override
    public Observable<ArrayList<City>> getResultsFromLocal(String country) {
        ArrayList<City> cities = new ArrayList<>();
        cities.add(new City("4f9eafc7-a451-4445-9614-8ff9b8017ba3", "7e5be4cf-2de0-4a46-9716-13177dac9dc9", "Managua"));
        cities.add(new City("4f9eafc7-a451-4445-9614-8ff9b8017ba3", "7e5be4cf-2de0-4a46-9716-13177dac9dc9", "Masaya"));
        cities.add(new City("4f9eafc7-a451-4445-9614-8ff9b8017ba3", "7e5be4cf-2de0-4a46-9716-13177dac9dc9", "Granada"));
        return Observable.just(cities);
    }
}
