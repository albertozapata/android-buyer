package com.delepues.buyer.address;

import com.delepues.buyer.http.BuyerApiService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by azapata on 10/12/17.
 */
@Module
public class AddressModule {
    @Provides
    public AddressFragmentMVP.Presenter providesAddressFragmentPresenter(AddressFragmentMVP.Model landingModel) {
        return new AddressPresenter(landingModel);
    }

    @Provides
    public AddressFragmentMVP.Model providesAddressFragmentModel(Repository repository) {
        return new AddressModel(repository);
    }

    @Singleton
    @Provides
    public Repository provideRepo(BuyerApiService buyerApiService) {
        return new AddressRepository(buyerApiService);
    }
}
