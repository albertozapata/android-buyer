package com.delepues.buyer.address;

import com.delepues.buyer.http.apimodel.City;
import com.delepues.buyer.http.apimodel.SectionDataModel;
import com.delepues.buyer.store.StoreFragmentMVP;

import java.util.ArrayList;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by azapata on 10/12/17.
 */

public class AddressPresenter implements AddressFragmentMVP.Presenter {
    private AddressFragmentMVP.View view;
    private Subscription subscription = null;
    private AddressFragmentMVP.Model model;
    private ArrayList<City> cities = new ArrayList<>();

    public AddressPresenter(AddressFragmentMVP.Model model) {
        this.model = model;
    }


    @Override
    public void getCities(String country) {
        subscription = model.getCities(country).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ArrayList<City>>() {
                    @Override
                    public void onCompleted() {
                        if (!cities.isEmpty())
                            view.showCities(cities);
                        else
                            view.errorEmpty();
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.errorCities(e.getMessage());
                    }

                    @Override
                    public void onNext(ArrayList<City> cities1) {
                        cities = cities1;
                    }
                });
    }

    @Override
    public void rxUnsubscribe() {
        if (subscription != null) {
            if (!subscription.isUnsubscribed()) {
                subscription.unsubscribe();
            }
        }
    }

    @Override
    public void setView(AddressFragmentMVP.View view) {
        this.view = view;
    }
}
