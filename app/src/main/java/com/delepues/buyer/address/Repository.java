package com.delepues.buyer.address;

import com.delepues.buyer.http.apimodel.City;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public interface Repository {
    Observable<ArrayList<City>> getResultsFromNetwork(String country);
    Observable<ArrayList<City>> getResultsFromLocal(String country);
}
