package com.delepues.buyer.address;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.delepues.buyer.R;
import com.delepues.buyer.http.apimodel.Address;
import com.delepues.buyer.register.RegisterFragment;
import com.delepues.buyer.utils.Constants;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.pixplicity.easyprefs.library.Prefs;
import com.satsuware.usefulviews.LabelledSpinner;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddressActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    AddressFragment addressFragment;
    MapAddressFragment mapAddressFragment;
    String location = "";
    String city = "";
    double latitude = 0;
    double longitude = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);

        ButterKnife.bind(this);

        addressFragment = AddressFragment.newInstance(Prefs.getString(Constants.codeCountry, ""));

        setUpToolbar();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.frame_main, addressFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void changeFragment(String address, String city) {
        this.location = address;
        this.city = city;
        mapAddressFragment = MapAddressFragment.newInstance(address);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.frame_main, mapAddressFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void resultAddress(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        Address address = new Address(location, longitude, latitude);
        address.city_id = city;
        Intent intent=new Intent();
        intent.putExtra(Constants.address,address);
        setResult(2,intent);
        finish();//finishing activity
    }

    private void setUpToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.title_landing_fragment);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
    }
}
