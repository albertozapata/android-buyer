package com.delepues.buyer.address;

import com.delepues.buyer.http.apimodel.City;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public interface AddressFragmentMVP {
    interface View {
        void showCities(ArrayList<City> cities);

        void errorCities(String s);

        void showProgress();

        void hideProgress();

        void errorEmpty();
    }

    interface Presenter {
        void getCities(String country);

        void rxUnsubscribe();

        void setView(AddressFragmentMVP.View view);
    }

    interface Model {
        Observable<ArrayList<City>> getCities(String country);
    }
}
