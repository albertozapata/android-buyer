package com.delepues.buyer.login;

import com.delepues.buyer.http.BuyerApiService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by azapata on 10/12/17.
 */
@Module
public class LoginModule {
    @Provides
    public LoginFragmentMVP.Presenter providesLoginFragmentPresenter(LoginFragmentMVP.Model landingModel) {
        return new LoginPresenter(landingModel);
    }

    @Provides
    public LoginFragmentMVP.Model provideLoginFragmentModel(Repository repository) {
        return new LoginModel(repository);
    }

    @Singleton
    @Provides
    public Repository provideRepo(BuyerApiService buyerApiService) {
        return new LoginRepository(buyerApiService);
    }
}
