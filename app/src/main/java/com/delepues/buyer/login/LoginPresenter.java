package com.delepues.buyer.login;

import com.delepues.buyer.http.apimodel.Category;
import com.delepues.buyer.http.apimodel.Login;
import com.delepues.buyer.http.apimodel.LoginResponse;
import com.delepues.buyer.http.apimodel.LoginSocial;

import java.util.ArrayList;

import retrofit2.Response;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by azapata on 10/12/17.
 */

public class LoginPresenter implements LoginFragmentMVP.Presenter {
    private LoginFragmentMVP.View view;
    private Subscription subscription = null;
    private LoginFragmentMVP.Model model;
    private LoginResponse response;

    public LoginPresenter(LoginFragmentMVP.Model model) {
        this.model = model;
    }


    @Override
    public void loginUser(Login login) {
        subscription = model.loginUser(login).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<LoginResponse>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.errorEmpty();
                    }

                    @Override
                    public void onNext(Response<LoginResponse> response) {
                        if (response.code() == 200) {
                            view.showUser(response.body());
                        } else if (response.code() == 400) {
                            view.errorLogin();
                        } else {
                            view.errorEmpty();
                        }
                    }
                });
    }

    @Override
    public void loginSocial(LoginSocial login) {
        subscription = model.loginSocial(login).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<LoginResponse>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.errorEmpty();
                    }

                    @Override
                    public void onNext(Response<LoginResponse> response) {
                        if (response.code() == 200) {
                            view.showUser(response.body());
                        } else if (response.code() == 400) {
                            view.errorLogin();
                        } else {
                            view.errorEmpty();
                        }
                    }
                });
    }

    @Override
    public void rxUnsubscribe() {
        if (subscription != null) {
            if (!subscription.isUnsubscribed()) {
                subscription.unsubscribe();
            }
        }
    }

    @Override
    public void setView(LoginFragmentMVP.View view) {
        this.view = view;
    }
}
