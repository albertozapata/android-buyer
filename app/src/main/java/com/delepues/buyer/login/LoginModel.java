package com.delepues.buyer.login;

import com.delepues.buyer.http.apimodel.Login;
import com.delepues.buyer.http.apimodel.LoginResponse;
import com.delepues.buyer.http.apimodel.LoginSocial;

import retrofit2.Response;
import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public class LoginModel implements LoginFragmentMVP.Model {
    private Repository repository;

    public LoginModel(Repository repository) {
        this.repository = repository;
    }

    @Override
    public Observable<Response<LoginResponse>> loginUser(Login login) {
        return repository.getResultsFromNetwork(login);
    }

    @Override
    public Observable<Response<LoginResponse>> loginSocial(LoginSocial login) {
        return repository.getResultsSocialFromNetwork(login);
    }
}
