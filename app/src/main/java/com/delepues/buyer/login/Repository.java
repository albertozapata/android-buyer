package com.delepues.buyer.login;

import com.delepues.buyer.http.apimodel.Category;
import com.delepues.buyer.http.apimodel.Login;
import com.delepues.buyer.http.apimodel.LoginResponse;
import com.delepues.buyer.http.apimodel.LoginSocial;

import java.util.ArrayList;

import retrofit2.Response;
import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public interface Repository {
    Observable<Response<LoginResponse>> getResultsFromNetwork(Login login);
    Observable<Response<LoginResponse>> getResultsFromLocal(Login login);
    Observable<Response<LoginResponse>> getResultsSocialFromNetwork(LoginSocial login);
}
