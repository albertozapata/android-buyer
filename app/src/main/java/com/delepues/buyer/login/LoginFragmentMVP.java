package com.delepues.buyer.login;

import com.delepues.buyer.http.apimodel.Login;
import com.delepues.buyer.http.apimodel.LoginResponse;
import com.delepues.buyer.http.apimodel.LoginSocial;
import com.delepues.buyer.http.apimodel.User;

import retrofit2.Response;
import rx.Observable;

/**
 * Created by azapata on 10/31/17.
 */

public interface LoginFragmentMVP {
    interface View {
        void showUser(LoginResponse response);

        void errorLogin();

        void showProgress();

        void hideProgress();

        void errorEmpty();
    }

    interface Presenter {
        void loginUser(Login login);

        void loginSocial(LoginSocial login);

        void rxUnsubscribe();

        void setView(LoginFragmentMVP.View view);
    }

    interface Model {
        Observable<Response<LoginResponse>> loginUser(Login login);

        Observable<Response<LoginResponse>> loginSocial(LoginSocial login);
    }
}
