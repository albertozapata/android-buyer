package com.delepues.buyer.login;

import com.delepues.buyer.http.BuyerApiService;
import com.delepues.buyer.http.apimodel.Login;
import com.delepues.buyer.http.apimodel.LoginResponse;
import com.delepues.buyer.http.apimodel.LoginSocial;
import com.delepues.buyer.http.apimodel.User;
import com.delepues.buyer.http.apimodel.UserGeneral;

import retrofit2.Response;
import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public class LoginRepository implements Repository {
    BuyerApiService buyerApiService;

    public LoginRepository(BuyerApiService buyerApiService) {
        this.buyerApiService = buyerApiService;
    }

    @Override
    public Observable<Response<LoginResponse>> getResultsFromNetwork(Login login) {
        return buyerApiService.loginUser("deb03f8e-8339-4410-89a2-671c2b25edce",login);
    }

    @Override
    public Observable<Response<LoginResponse>> getResultsFromLocal(Login login) {
        User user = new User();
        user.general = new UserGeneral("a144693c-face-4b8f-8ddc-b2acf0df80e0", "369fb8be-885c-471a-8dbf-00b4664b6028", "maaburto", "moises@kokosushi.com", "Alejo", "Aburto", "Me like Test");
        user.user_profile_id = "c2e0800c-becb-4906-9ab4-1e3adc07ec57";
        user.profile = "Admin";
        LoginResponse loginResponse = new LoginResponse(user, "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJfaWQiOiJhMTQ0NjkzYy1mYWNlLTRiOGYtOGRkYy1iMmFjZjBkZjgwZTAiLCJwcm9maWxlIjoiQWRtaW4ifSwiaWF0IjoxNTA5NDcwNjkxLCJleHAiOjE1MTAwNzU0OTF9.dMw1q_Gebv9ATFjvC_wJLxAShtfGLDHlQLjeR25Hy-o", true);
        Response<LoginResponse> log = Response.success(loginResponse);

        return Observable.just(log);
    }

    @Override
    public Observable<Response<LoginResponse>> getResultsSocialFromNetwork(LoginSocial login) {
        return buyerApiService.loginSocial("deb03f8e-8339-4410-89a2-671c2b25edce",login);
    }
}
