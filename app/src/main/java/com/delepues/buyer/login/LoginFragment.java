package com.delepues.buyer.login;


import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.delepues.buyer.R;
import com.delepues.buyer.http.apimodel.Address;
import com.delepues.buyer.http.apimodel.Login;
import com.delepues.buyer.http.apimodel.LoginResponse;
import com.delepues.buyer.http.apimodel.LoginSocial;
import com.delepues.buyer.http.apimodel.Social;
import com.delepues.buyer.register.RegisterActivity;
import com.delepues.buyer.root.App;
import com.delepues.buyer.utils.Constants;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.basgeekball.awesomevalidation.ValidationStyle.BASIC;
import static com.basgeekball.awesomevalidation.ValidationStyle.UNDERLABEL;

public class LoginFragment extends Fragment implements LoginFragmentMVP.View {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    @BindView(R.id.edit_user_login)
    EditText userName;

    @BindView(R.id.edit_pass_login)
    EditText password;

    @BindView(R.id.button_login)
    Button login;

    @BindView(R.id.button_login_facebook)
    Button loginFb;

    @BindView(R.id.text_login_register)
    TextView register;

    @Inject
    LoginFragmentMVP.Presenter presenter;

    private AwesomeValidation mAwesomeValidation;
    private LoginButton buttonFb;
    CallbackManager callbackManager;

    public LoginFragment() {
        // Required empty public constructor
    }

    public static LoginFragment newInstance(String param1, String param2) {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getActivity().getApplication()).getComponent().inject(this);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        buttonFb = view.findViewById(R.id.login_button);
        ButterKnife.bind(this, view);

        buttonFb.setReadPermissions(Arrays.asList("email", "public_profile"));
        buttonFb.setFragment(this);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        callbackManager = CallbackManager.Factory.create();


        // init validator
        mAwesomeValidation = new AwesomeValidation(BASIC);
        //mAwesomeValidation.setContext(getContext());  // mandatory for UNDERLABEL style
        mAwesomeValidation.addValidation(getActivity(), R.id.edit_user_login, ".{8,}", R.string.error_email);
        mAwesomeValidation.addValidation(getActivity(), R.id.edit_pass_login, ".{8,}", R.string.error_password);

        // Callback registration
        buttonFb.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Log.e("DELEPUES", String.format("Entro a fb: %s", loginResult.getAccessToken().getToken()));
                Address address = new Address("undefined", 0, 0);
                Social social = new Social(address, "undefined", "facebook", loginResult.getAccessToken().getToken());
                presenter.loginSocial(new LoginSocial(social));
            }

            @Override
            public void onCancel() {
                // App code
                Log.e("DELEPUES", "Se cancelo");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.e("DELEPUES", String.format("Error a fb: %s", exception.toString()));
            }
        });

        loginFb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonFb.performClick();
            }
        });

        register.setPaintFlags(register.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        register.setText("Crear Cuenta");
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((RegisterActivity) getActivity()).loadRegisterFrag();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void showUser(LoginResponse response) {
        Prefs.putString(Constants.token, response.token);
        Prefs.putString(Constants.user, response.user.general.username);
        Prefs.putString(Constants.email, response.user.general.email);
        //Prefs.putString(Constants.phone, response.user.general);
        ((RegisterActivity) getActivity()).loadBuyer();
    }

    @Override
    public void errorLogin() {
        Log.e("DELEPUES", "errorLogin");
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void errorEmpty() {
        Log.e("DELEPUES", "errorEmpty");
    }

    @OnClick(R.id.button_login)
    public void submit(View view) {
        if (mAwesomeValidation.validate()) {
            String user = userName.getText().toString().trim();
            String pass = password.getText().toString().trim();
            pass = com.delepues.buyer.utils.ExtensionsKt.getSHA(pass, pass);
            pass = pass != null ? pass.replace(System.getProperty("line.separator"), "") : "";
            presenter.loginUser(new Login(user, pass));
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.setView(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.rxUnsubscribe();
    }
}
