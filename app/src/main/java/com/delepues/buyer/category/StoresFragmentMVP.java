package com.delepues.buyer.category;

import com.delepues.buyer.http.apimodel.Branch;
import com.delepues.buyer.http.apimodel.Category;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public interface StoresFragmentMVP {
    interface View {
        void showBranches(ArrayList<Branch> branches);

        void errorBranches(String s);

        void showProgress();

        void hideProgress();

        void errorEmpty();
    }

    interface Presenter {
        void getBranches(double latitude,double longitude,String categoryId);

        void rxUnsubscribe();

        void setView(StoresFragmentMVP.View view);
    }

    interface Model {
        Observable<ArrayList<Branch>> getBranches(double latitude,double longitude,String categoryId);
    }
}
