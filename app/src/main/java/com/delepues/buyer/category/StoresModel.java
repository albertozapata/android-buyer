package com.delepues.buyer.category;

import com.delepues.buyer.http.apimodel.Branch;
import com.delepues.buyer.http.apimodel.Category;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public class StoresModel implements StoresFragmentMVP.Model {
    private Repository repository;

    public StoresModel(Repository repository) {
        this.repository = repository;
    }

    @Override
    public Observable<ArrayList<Branch>> getBranches(double latitude, double longitude, String categoryId) {
        return repository.getResultsFromNetwork(latitude, longitude, categoryId);
        //return repository.getResultsFromLocal(latitude, longitude, categoryId);
    }
}
