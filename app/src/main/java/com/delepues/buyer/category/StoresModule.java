package com.delepues.buyer.category;

import com.delepues.buyer.http.BuyerApiService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by azapata on 10/12/17.
 */
@Module
public class StoresModule {
    @Provides
    public StoresFragmentMVP.Presenter providesLandingFragmentPresenter(StoresFragmentMVP.Model landingModel) {
        return new StoresPresenter(landingModel);
    }

    @Provides
    public StoresFragmentMVP.Model provideLandingFragmentModel(Repository repository) {
        return new StoresModel(repository);
    }

    @Singleton
    @Provides
    public Repository provideRepo(BuyerApiService buyerApiService) {
        return new StoresRepository(buyerApiService);
    }
}
