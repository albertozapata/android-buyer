package com.delepues.buyer.category;

import com.delepues.buyer.http.apimodel.Branch;
import com.delepues.buyer.http.apimodel.Category;

import java.util.ArrayList;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by azapata on 10/12/17.
 */

public class StoresPresenter implements StoresFragmentMVP.Presenter {
    private StoresFragmentMVP.View view;
    private Subscription subscription = null;
    private StoresFragmentMVP.Model model;
    private ArrayList<Branch> branches = new ArrayList<>();

    public StoresPresenter(StoresFragmentMVP.Model model) {
        this.model = model;
    }

    @Override
    public void getBranches(double latitude, double longitude, String categoryId) {
        subscription = model.getBranches(latitude, longitude, categoryId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ArrayList<Branch>>() {
                    @Override
                    public void onCompleted() {
                        if (!branches.isEmpty())
                            view.showBranches(branches);
                        else
                            view.errorEmpty();
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.errorBranches(e.getMessage());
                    }

                    @Override
                    public void onNext(ArrayList<Branch> brancheList) {
                        branches = brancheList;
                    }
                });
    }

    @Override
    public void rxUnsubscribe() {
        if (subscription != null) {
            if (!subscription.isUnsubscribed()) {
                subscription.unsubscribe();
            }
        }
    }

    @Override
    public void setView(StoresFragmentMVP.View view) {
        this.view = view;
    }
}
