package com.delepues.buyer.category;

import com.delepues.buyer.http.apimodel.Branch;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public interface Repository {
    Observable<ArrayList<Branch>> getResultsFromNetwork(double latitude,double longitude,String categoryId);
    Observable<ArrayList<Branch>> getResultsFromLocal(double latitude,double longitude,String categoryId);
}
