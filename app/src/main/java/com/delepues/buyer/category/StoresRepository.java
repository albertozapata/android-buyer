package com.delepues.buyer.category;

import com.delepues.buyer.http.BuyerApiService;
import com.delepues.buyer.http.apimodel.Branch;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public class StoresRepository implements Repository {
    BuyerApiService buyerApiService;

    public StoresRepository(BuyerApiService buyerApiService) {
        this.buyerApiService = buyerApiService;
    }

    @Override
    public Observable<ArrayList<Branch>> getResultsFromNetwork(double latitude, double longitude, String categoryId) {
        return buyerApiService.storeBranches("deb03f8e-8339-4410-89a2-671c2b25edce", categoryId, latitude, longitude);
    }

    @Override
    public Observable<ArrayList<Branch>> getResultsFromLocal(double latitude, double longitude, String categoryId) {
        ArrayList<Branch> branches = new ArrayList<>();
        branches.add(new Branch("1", "La Colonia", "La Colonia", "http://www.launiversal.com.ni/wp-content/uploads/2015/07/0000_clients_lacolonia_launiversal.png", "http://www.lacolonia.com.ni/slc_admin/ARCHIVOS/GALERIA/blogpost-calidad-verduras2.png", "08:00", "21:00", "Supermercado la colonia", "2.3 km", "1"));
        branches.add(new Branch("2", "La Union", "La Union", "https://nuevaya.com.ni/wp-content/uploads/2014/09/images1.jpg", "https://drive.google.com/open?id=1Ii1-GTOGBn-hCtk34PPjNZtoU1b9jEfh", "08:00", "22:00", "Supermercado la union", "3.0 km", "2"));
        branches.add(new Branch("3", "Pali", "Pali", "http://1.bp.blogspot.com/-5Nf-7dNkEZg/UXEM8Nm0XUI/AAAAAAAALBc/kkYiLqa8ChU/s1600/logo_under.png", "https://drive.google.com/open?id=1Ii1-GTOGBn-hCtk34PPjNZtoU1b9jEfh", "08:00", "20:00", "Supermercado pali", "3.0 km", "3"));
        branches.add(new Branch("4", "Maxi Pali", "Maxi Pali", "http://tintesgallo.com/wp-content/uploads/2015/07/Maxi-Pali.png", "https://drive.google.com/open?id=1Ii1-GTOGBn-hCtk34PPjNZtoU1b9jEfh", "08:00", "21:00", "Supermercado maxi pali", "3.3 km", "4"));
        branches.add(new Branch("5", "Walmart", "Walmart", "https://upload.wikimedia.org/wikipedia/commons/thumb/c/ca/Walmart_logo.svg/800px-Walmart_logo.svg.png", "https://drive.google.com/open?id=1Ii1-GTOGBn-hCtk34PPjNZtoU1b9jEfh", "09:00", "22:00", "Supermercado walmart", "4.0 km", "5"));
        return Observable.just(branches);
    }
}
