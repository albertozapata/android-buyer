package com.delepues.buyer.category

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.delepues.buyer.R
import com.delepues.buyer.http.apimodel.Branch
import com.delepues.buyer.utils.loadUrl
import de.hdodenhof.circleimageview.CircleImageView
import java.util.*

/**
 * Created by azapata on 10/10/17.
 */

class StoresAdapter(private val dataSet: ArrayList<Branch>, private val listener: StoresAdapter.OnItemClickListener) : RecyclerView.Adapter<StoresAdapter.MyViewHolder>() {

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        internal var textViewName: TextView = itemView.findViewById<View>(R.id.text_item_store_name) as TextView
        internal var textViewPrice: TextView = itemView.findViewById<View>(R.id.text_item_store_price) as TextView
        internal var textViewMin: TextView = itemView.findViewById<View>(R.id.text_item_store_min) as TextView
        internal var imageViewIcon: CircleImageView = itemView.findViewById<View>(R.id.image_item_store) as CircleImageView

        fun bind(item: Branch, listener: OnItemClickListener) {
            textViewName.text = item.store_branch_name
            textViewPrice.text = item.open_time
            textViewMin.text = item.close_time
            itemView.setOnClickListener {
                listener.onItemClick(item)
            }
            imageViewIcon.loadUrl(item.image_url)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_stores, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, listPosition: Int) {
        holder.bind(dataSet[listPosition], listener)
    }

    override fun getItemCount(): Int = dataSet.size

    interface OnItemClickListener {
        fun onItemClick(item: Branch)
    }
}