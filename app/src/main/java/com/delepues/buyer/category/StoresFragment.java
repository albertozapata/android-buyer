package com.delepues.buyer.category;


import android.media.Image;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.TextView;

import com.delepues.buyer.MainActivity;
import com.delepues.buyer.R;
import com.delepues.buyer.http.apimodel.Branch;
import com.delepues.buyer.http.apimodel.Category;
import com.delepues.buyer.root.App;
import com.delepues.buyer.utils.Constants;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StoresFragment extends Fragment implements StoresFragmentMVP.View {
    private double latitude;
    private double longitude;
    private Category category;

    @BindView(R.id.recycler_stores)
    RecyclerView recyclerView;
    @BindView(R.id.collapsingToolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.toolbarImage)
    ImageView toolbarImage;
    @BindView(R.id.text_result_stores)
    TextView error;

    @Inject
    StoresFragmentMVP.Presenter presenter;

    public StoresFragment() {
        // Required empty public constructor
    }

    public static StoresFragment newInstance(double latitude, double longitude, Category category) {
        StoresFragment fragment = new StoresFragment();
        Bundle args = new Bundle();
        args.putDouble(Constants.latitude, latitude);
        args.putDouble(Constants.latitude, longitude);
        args.putParcelable(Constants.category, category);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getActivity().getApplication()).getComponent().inject(this);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            latitude = getArguments().getDouble(Constants.latitude);
            longitude = getArguments().getDouble(Constants.latitude);
            category = getArguments().getParcelable(Constants.category);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_stores, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getActivity(), resId);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutAnimation(animation);

        Picasso.with(getContext()).load(category.image_url).placeholder(getResources().getDrawable(R.drawable.banner_placeholder)).into(toolbarImage);
        collapsingToolbarLayout.setTitle(category.name);
        presenter.getBranches(latitude, longitude, category.id);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void showBranches(ArrayList<Branch> branches) {
        RecyclerView.Adapter adapter = new StoresAdapter(branches, new StoresAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(@NotNull Branch item) {
                ((MainActivity) getActivity()).changeFragment("Branch", item);
            }
        });

        recyclerView.setAdapter(adapter);
        recyclerView.getAdapter().notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();

    }

    @Override
    public void errorBranches(String s) {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void errorEmpty() {
        error.setVisibility(View.VISIBLE);
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.setView(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.rxUnsubscribe();
    }
}
