package com.delepues.buyer.landing

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.delepues.buyer.R
import com.delepues.buyer.http.apimodel.Order
import com.delepues.buyer.http.apimodel.OrderDetail
import com.delepues.buyer.utils.loadUrl
import com.makeramen.roundedimageview.RoundedImageView
import java.util.*
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView


/**
 * Created by azapata on 10/10/17.
 */

class InvoiceDetailAdapter(private val dataSet: ArrayList<OrderDetail>, private val context: Context) : RecyclerView.Adapter<InvoiceDetailAdapter.MyViewHolder>() {

    //var vectorList = intArrayOf(R.drawable.super_mercado_vector, R.drawable.farmacia_vector, R.drawable.restaurante_vector, R.drawable.tienda_vector)

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        internal var name: TextView = itemView.findViewById<View>(R.id.text_item_product) as TextView
        internal var price: TextView = itemView.findViewById<View>(R.id.text_item_price) as TextView

        fun bind(item: OrderDetail) {
            name.text = item.name
            price.text = String.format("C$ %.2f", item.price)
            //imageViewIcon.setImageDrawable(context.resources.getDrawable(vectorList[listPosition]))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_invoice, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, listPosition: Int) {

        holder.bind(dataSet[listPosition])
    }

    override fun getItemCount(): Int = dataSet.size
}
