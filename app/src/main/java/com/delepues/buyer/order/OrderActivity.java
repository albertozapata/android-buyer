package com.delepues.buyer.order;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.delepues.buyer.R;
import com.delepues.buyer.http.apimodel.Order;
import com.delepues.buyer.utils.Constants;

import java.net.URISyntaxException;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class OrderActivity extends AppCompatActivity {

    @BindView(R.id.tabs)
    TabLayout tabLayout;

    @BindView(R.id.view_pager_order)
    ViewPager viewPager;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    Order order = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        ButterKnife.bind(this);
        order = getIntent().getExtras().getParcelable(Constants.order);
        setUpToolbar();
        buildTabs();

    }

    private void buildTabs() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new StatusFragment(), "ORDEN");
        adapter.addFragment(InvoiceFragment.newInstance(order), "DETALLE");
        viewPager.setAdapter(adapter);

        tabLayout.setupWithViewPager(viewPager);
    }

    private void setUpToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.title_landing_fragment));
    }

}
