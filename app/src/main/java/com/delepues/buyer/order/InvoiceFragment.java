package com.delepues.buyer.order;


import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.delepues.buyer.R;
import com.delepues.buyer.http.apimodel.Order;
import com.delepues.buyer.landing.InvoiceDetailAdapter;
import com.delepues.buyer.utils.Constants;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class InvoiceFragment extends Fragment {

    Order order;
    @BindView(R.id.text_subtotal_invoice)
    TextView subTotal;
    @BindView(R.id.text_delivery_invoice)
    TextView delivery;
    @BindView(R.id.text_total_invoice)
    TextView total;
    @BindView(R.id.text_store_name)
    TextView store;

    @BindView(R.id.image_user_invoice)
    ImageView userImage;
    @BindView(R.id.image_delivery_invoice)
    ImageView deliveryImage;
    @BindView(R.id.image_destination_invoice)
    ImageView destinationImage;
    @BindView(R.id.list_products_invoice)
    RecyclerView recyclerView;

    @BindView(R.id.relative_delivery)
    RelativeLayout relativeLayout;

    //    @BindView(R.id.image_store_invoice)
    CircleImageView branchImage;

    public InvoiceFragment() {
        // Required empty public constructor
    }

    public static InvoiceFragment newInstance(Order order) {
        InvoiceFragment fragment = new InvoiceFragment();
        Bundle args = new Bundle();
        args.putParcelable(Constants.order, order);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            order = getArguments().getParcelable(Constants.order);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_invoice, container, false);
        branchImage = view.findViewById(R.id.image_store_invoice);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        store.setText(order.branch_name);
        Picasso.with(getContext()).load(order.branch_image_url).into(branchImage);
        int color = getActivity().getResources().getColor(R.color.colorPrimary);
        Drawable drawable = new IconicsDrawable(getContext())
                .icon(FontAwesome.Icon.faw_user)
                .color(color)
                .sizeDp(24);
        userImage.setImageDrawable(drawable);

        drawable = new IconicsDrawable(getContext())
                .icon(FontAwesome.Icon.faw_bicycle)
                .color(color)
                .sizeDp(24);
        deliveryImage.setImageDrawable(drawable);

        drawable = new IconicsDrawable(getContext())
                .icon(FontAwesome.Icon.faw_road)
                .color(color)
                .sizeDp(24);
        destinationImage.setImageDrawable(drawable);

        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getActivity(), resId);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutAnimation(animation);

        RecyclerView.Adapter adapter = new InvoiceDetailAdapter(order.details, getActivity());
        recyclerView.setAdapter(adapter);
        recyclerView.getAdapter().notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();

        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MapActivity.class);
                startActivity(intent);
            }
        });
    }
}
