package com.delepues.buyer.register;

import com.delepues.buyer.http.apimodel.Buyer;
import com.delepues.buyer.http.apimodel.BuyerResponse;

import retrofit2.Response;
import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public interface Repository {
    Observable<BuyerResponse> getResultsFromNetwork(Buyer buyer);

    Observable<BuyerResponse> getResultsFromLocal(Buyer buyer);
}
