package com.delepues.buyer.register;

import com.delepues.buyer.http.apimodel.Buyer;
import com.delepues.buyer.http.apimodel.BuyerResponse;

import retrofit2.Response;
import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public interface RegisterFragmentMVP {
    interface View {
        void showRegister(BuyerResponse response);

        void errorRegister(String s);

        void showProgress();

        void hideProgress();

        void errorEmpty();
    }

    interface Presenter {
        void registerBuyer(Buyer buyer);

        void rxUnsubscribe();

        void setView(RegisterFragmentMVP.View view);
    }

    interface Model {
        Observable<BuyerResponse> registerBuyer(Buyer buyer);
    }
}
