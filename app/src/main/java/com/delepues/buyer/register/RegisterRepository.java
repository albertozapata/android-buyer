package com.delepues.buyer.register;

import com.delepues.buyer.http.BuyerApiService;
import com.delepues.buyer.http.apimodel.Buyer;
import com.delepues.buyer.http.apimodel.BuyerResponse;

import java.util.ArrayList;

import retrofit2.Response;
import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public class RegisterRepository implements Repository {
    BuyerApiService buyerApiService;

    public RegisterRepository(BuyerApiService buyerApiService) {
        this.buyerApiService = buyerApiService;
    }

    @Override
    public Observable<BuyerResponse> getResultsFromNetwork(Buyer buyer) {
        return buyerApiService.register("deb03f8e-8339-4410-89a2-671c2b25edce",buyer);
    }

    @Override
    public Observable<BuyerResponse> getResultsFromLocal(Buyer buyer) {
        BuyerResponse buyerResponse = new BuyerResponse();
        buyerResponse.user_profile_id = "1";
        buyerResponse.id = "123";
        buyerResponse.username = buyer.username;
        buyerResponse.phone = buyer.phone;
        buyerResponse.email = buyer.email;
        return Observable.just(buyerResponse);
    }


/*
* {
    "user_profile_id": "943051ca-8e7b-42d6-997c-c9f830c190d8",
    "id": "b936de04-08a5-43fb-add0-4729058f517c",
    "username": "aaaffsaas",
    "first_name": "aaa",
    "last_name": "aaa",
    "phone": "aaa",
    "email": "aaa"
}
*
* */
}
