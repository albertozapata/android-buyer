package com.delepues.buyer.register;

import com.delepues.buyer.http.BuyerApiService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by azapata on 10/12/17.
 */
@Module
public class RegisterModule {
    @Provides
    public RegisterFragmentMVP.Presenter providesRegisterFragmentPresenter(RegisterFragmentMVP.Model landingModel) {
        return new RegisterPresenter(landingModel);
    }

    @Provides
    public RegisterFragmentMVP.Model provideRegisterFragmentModel(Repository repository) {
        return new RegisterModel(repository);
    }

    @Singleton
    @Provides
    public Repository provideRepo(BuyerApiService buyerApiService) {
        return new RegisterRepository(buyerApiService);
    }
}
