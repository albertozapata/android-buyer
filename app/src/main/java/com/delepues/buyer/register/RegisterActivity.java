package com.delepues.buyer.register;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.delepues.buyer.R;
import com.delepues.buyer.address.AddressActivity;
import com.delepues.buyer.http.apimodel.Address;
import com.delepues.buyer.http.apimodel.Buyer;
import com.delepues.buyer.http.apimodel.BuyerResponse;
import com.delepues.buyer.login.LoginFragment;
import com.delepues.buyer.root.App;
import com.delepues.buyer.utils.Constants;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegisterActivity extends AppCompatActivity implements RegisterFragmentMVP.View {

    RegisterFragment registerFragment;
    LoginFragment loginFragment;
    Address address;
    @Inject
    RegisterFragmentMVP.Presenter presenter;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        setUpToolbar();
        ((App) getApplication()).getComponent().inject(this);
        loginFragment = new LoginFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.frame_main, loginFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void loadAddressFrag() {
        Intent intent = new Intent(RegisterActivity.this, AddressActivity.class);
        startActivityForResult(intent, 2);// Activity is started with requestCode 2
    }

    public void loadRegisterFrag() {
        registerFragment = new RegisterFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.frame_main, registerFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void loadBuyer() {
        finish();
    }

    public void registerUser(Buyer buyer) {
        buyer.address = address;
        buyer.image_url = "image";
        buyer.image_file_name = "image";
        presenter.registerBuyer(buyer);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == 2) {
            address = data.getParcelableExtra(Constants.address);
            registerFragment.setAddress(address.location);
        }
    }

    @Override
    public void showRegister(BuyerResponse response) {
        finish();
    }

    @Override
    public void errorRegister(String s) {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void errorEmpty() {

    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.setView(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.rxUnsubscribe();
    }

    private void setUpToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.title_landing_fragment));
    }


}
