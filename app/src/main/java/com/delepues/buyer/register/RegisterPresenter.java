package com.delepues.buyer.register;

import com.delepues.buyer.http.apimodel.Buyer;
import com.delepues.buyer.http.apimodel.BuyerResponse;
import com.delepues.buyer.http.apimodel.Category;
import com.delepues.buyer.landing.LandingFragmentMVP;

import java.util.ArrayList;

import retrofit2.Response;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by azapata on 10/12/17.
 */

public class RegisterPresenter implements RegisterFragmentMVP.Presenter {
    private RegisterFragmentMVP.View view;
    private Subscription subscription = null;
    private RegisterFragmentMVP.Model model;
    private BuyerResponse buyerResponse;

    public RegisterPresenter(RegisterFragmentMVP.Model model) {
        this.model = model;
    }

    @Override
    public void registerBuyer(final Buyer buyer) {
        subscription = model.registerBuyer(buyer).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BuyerResponse>() {
                    @Override
                    public void onCompleted() {
                        if (buyerResponse == null) {
                            view.errorEmpty();
                        } else {
                            view.showRegister(buyerResponse);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.errorRegister(e.getMessage());
                    }

                    @Override
                    public void onNext(BuyerResponse buyerResponseResponse) {
                        buyerResponse = buyerResponse;
                    }
                });
    }

    @Override
    public void rxUnsubscribe() {
        if (subscription != null) {
            if (!subscription.isUnsubscribed()) {
                subscription.unsubscribe();
            }
        }
    }

    @Override
    public void setView(RegisterFragmentMVP.View view) {
        this.view = view;
    }
}
