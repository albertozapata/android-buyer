package com.delepues.buyer.register;


import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.delepues.buyer.R;
import com.delepues.buyer.http.apimodel.Buyer;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;

import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.basgeekball.awesomevalidation.ValidationStyle.TEXT_INPUT_LAYOUT;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RegisterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RegisterFragment extends Fragment {

    @BindView(R.id.edit_name_register)
    TextInputEditText name;
    @BindView(R.id.edit_last_register)
    TextInputEditText lastName;
    @BindView(R.id.edit_username_register)
    TextInputEditText username;
    @BindView(R.id.edit_email_register)
    TextInputEditText email;
    @BindView(R.id.edit_phone_register)
    TextInputEditText phone;
    @BindView(R.id.edit_pass_register)
    TextInputEditText pass;
    @BindView(R.id.edit_confirm_register)
    TextInputEditText confirmPass;
    @BindView(R.id.relative_address)
    RelativeLayout relativeLayout;
    @BindView(R.id.text_label_register)
    TextView label;
    @BindView(R.id.text_address_register)
    TextView address;
    @BindView(R.id.image_add)
    ImageView imageAdd;

    @BindView(R.id.image_user_register)
    ImageView imageUser;
    @BindView(R.id.image_user_email)
    ImageView imageEmail;
    @BindView(R.id.image_phone_register)
    ImageView imagePhone;
    @BindView(R.id.image_pass_register)
    ImageView imagePass;
    @BindView(R.id.image_confirm_register)
    ImageView imageConfirm;
    @BindView(R.id.image_register_camera)
    ImageView imageCamera;
    @BindView(R.id.image_last_register)
    ImageView imageLastName;
    @BindView(R.id.image_name_register)
    ImageView imageName;

    private AwesomeValidation mAwesomeValidation;

    public RegisterFragment() {
        // Required empty public constructor
    }

    public static RegisterFragment newInstance() {
        RegisterFragment fragment = new RegisterFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        buildIcons();
        // init validator
        mAwesomeValidation = new AwesomeValidation(TEXT_INPUT_LAYOUT);
        //mAwesomeValidation.setContext(getContext());  // mandatory for UNDERLABEL style
        mAwesomeValidation.addValidation(getActivity(), R.id.text_input_name, ".{2,}", R.string.error_name);
        mAwesomeValidation.addValidation(getActivity(), R.id.text_input_last, ".{2,}", R.string.error_last);
        mAwesomeValidation.addValidation(getActivity(), R.id.text_input_username, ".{8,}", R.string.error_user);
        mAwesomeValidation.addValidation(getActivity(), R.id.text_input_email, Patterns.EMAIL_ADDRESS, R.string.error_email);
        mAwesomeValidation.addValidation(getActivity(), R.id.text_input_pass, ".{8,}", R.string.error_password);
        mAwesomeValidation.addValidation(getActivity(), R.id.text_input_confirm, R.id.text_input_pass, R.string.error_password);
        mAwesomeValidation.addValidation(getActivity(), R.id.text_input_phone, RegexTemplate.TELEPHONE, R.string.error_password);

        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((RegisterActivity) getActivity()).loadAddressFrag();
            }
        });
    }

    private void buildIcons() {
        int color = getActivity().getResources().getColor(R.color.colorGray);

        Drawable drawable = new IconicsDrawable(getContext())
                .icon(FontAwesome.Icon.faw_user)
                .color(color)
                .sizeDp(24);
        imageUser.setImageDrawable(drawable);
        drawable = new IconicsDrawable(getContext())
                .icon(FontAwesome.Icon.faw_phone)
                .color(color)
                .sizeDp(24);
        imagePhone.setImageDrawable(drawable);
        drawable = new IconicsDrawable(getContext())
                .icon(FontAwesome.Icon.faw_envelope)
                .color(color)
                .sizeDp(24);
        imageEmail.setImageDrawable(drawable);
        drawable = new IconicsDrawable(getContext())
                .icon(FontAwesome.Icon.faw_lock)
                .color(color)
                .sizeDp(24);
        imagePass.setImageDrawable(drawable);
        imageConfirm.setImageDrawable(drawable);
        drawable = new IconicsDrawable(getContext())
                .icon(FontAwesome.Icon.faw_camera)
                .color(color)
                .sizeDp(24);
        imageCamera.setImageDrawable(drawable);
        drawable = new IconicsDrawable(getContext())
                .icon(FontAwesome.Icon.faw_plus_circle)
                .color(getActivity().getResources().getColor(R.color.colorPrimary))
                .sizeDp(24);
        imageAdd.setImageDrawable(drawable);
        drawable = new IconicsDrawable(getContext())
                .icon(FontAwesome.Icon.faw_id_card)
                .color(getActivity().getResources().getColor(R.color.colorPrimary))
                .sizeDp(24);
        imageName.setImageDrawable(drawable);
        imageLastName.setImageDrawable(drawable);
    }

    @OnClick(R.id.button_register)
    public void submit() {
        if (mAwesomeValidation.validate()) {
            Buyer buyer = new Buyer();
            buyer.username = username.getText().toString().trim();
            buyer.password = pass.getText().toString().trim();
            buyer.email = email.getText().toString().trim();
            buyer.phone = phone.getText().toString().trim();
            buyer.first_name = name.getText().toString().trim();
            buyer.last_name = lastName.getText().toString().trim();
            ((RegisterActivity) getActivity()).registerUser(buyer);
        }
    }

    public void setAddress(String s) {
        label.setVisibility(View.INVISIBLE);
        imageAdd.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.vector_direction));
        address.setText(s);
    }
}
