package com.delepues.buyer.register;

import com.delepues.buyer.http.apimodel.Buyer;
import com.delepues.buyer.http.apimodel.BuyerResponse;
import com.delepues.buyer.http.apimodel.Category;

import java.util.ArrayList;

import retrofit2.Response;
import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public class RegisterModel implements RegisterFragmentMVP.Model {
    private Repository repository;

    public RegisterModel(Repository repository) {
        this.repository = repository;
    }

    @Override
    public Observable<BuyerResponse> registerBuyer(Buyer buyer) {
        return repository.getResultsFromNetwork(buyer);
    }
}
