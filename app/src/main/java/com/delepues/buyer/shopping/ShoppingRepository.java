package com.delepues.buyer.shopping;

import com.delepues.buyer.http.apimodel.Option;
import com.delepues.buyer.http.apimodel.OptionDetail;
import com.delepues.buyer.http.localmodel.Product;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by azapata on 10/12/17.
 */

public class ShoppingRepository implements Repository {
    private ArrayList<com.delepues.buyer.http.apimodel.Product> products;

    @Override
    public ArrayList<com.delepues.buyer.http.apimodel.Product> getProducts() {
        Realm realm = Realm.getDefaultInstance();
        products = new ArrayList<>();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmQuery<Product> query = realm.where(Product.class);
                RealmResults<Product> result = query.findAll();

                for (Product product : result) {
                    ArrayList<OptionDetail> optionDetails;
                    ArrayList<Option> options;
                    com.delepues.buyer.http.apimodel.Product product1 = new com.delepues.buyer.http.apimodel.Product();
                    product1.store_branch_product_id = product.store_branch_product_id;
                    product1.name = product.name;
                    product1.price = product.price;
                    product1.image_url = product.image_url;
                    product1.description = product.description;
                    product1.qty = product.qty;
                    options = new ArrayList<>();

                    for (com.delepues.buyer.http.localmodel.Option option : product.options) {
                        Option option1 = new Option();
                        option1.option_id = option.option_id;
                        option1.name = option.name;
                        option1.required = option.required;
                        option1.multichoice = option.multichoice;
                        optionDetails = new ArrayList<>();

                        for (com.delepues.buyer.http.localmodel.OptionDetail optionDetail : option.option_details) {
                            OptionDetail optionDetail1 = new OptionDetail();
                            optionDetail1.option_detail_id = optionDetail.option_detail_id;
                            optionDetail1.name = optionDetail.name;
                            optionDetail1.price = optionDetail.price;
                            optionDetail1.selected = optionDetail.selected;
                            optionDetails.add(optionDetail1);
                        }
                        option1.option_details = optionDetails;
                        options.add(option1);
                    }
                    product1.options = options;
                    products.add(product1);
                }
            }
        });
        return products;
    }
}
