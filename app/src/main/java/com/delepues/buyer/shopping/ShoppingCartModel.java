package com.delepues.buyer.shopping;

import com.delepues.buyer.http.apimodel.Product;

import java.util.ArrayList;

/**
 * Created by azapata on 10/12/17.
 */

public class ShoppingCartModel implements ShoppingCartFragmentMVP.Model {
    private Repository repository;

    public ShoppingCartModel(Repository repository) {
        this.repository = repository;
    }

    @Override
    public ArrayList<Product> getProduct() {
        return repository.getProducts();
    }
}
