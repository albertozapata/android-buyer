package com.delepues.buyer.shopping;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.delepues.buyer.R;
import com.delepues.buyer.http.apimodel.Product;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by azapata on 11/5/17.
 */

public class ShoppingCartAdapter extends BaseAdapter {

    private ArrayList<Product> dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView title;
        TextView desciption;
        TextView price;
        ImageView image;
    }

    public ShoppingCartAdapter(ArrayList<Product> data, Context context) {
        this.dataSet = data;
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return dataSet.size();
    }

    @Override
    public Product getItem(int position) {
        return dataSet.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Product dataModel = getItem(position);
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.item_shopping_cart, parent, false);
            viewHolder.title = convertView.findViewById(R.id.text_title_shopping);
            viewHolder.desciption = convertView.findViewById(R.id.text_descrip_shopping);
            viewHolder.price = convertView.findViewById(R.id.text_price_shopping);
            viewHolder.image = convertView.findViewById(R.id.image_item_shopping);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.title.setText(dataModel.name);
        viewHolder.desciption.setText(dataModel.description);
        viewHolder.price.setText(String.format(Locale.getDefault(), "%.2f", dataModel.price));

        Picasso.with(mContext).load(dataModel.image_url).into(viewHolder.image);
        // Return the completed view to render on screen
        return convertView;
    }

    public ArrayList<Product> getDataSet() {
        return dataSet;
    }
}
