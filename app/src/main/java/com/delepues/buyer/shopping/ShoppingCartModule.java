package com.delepues.buyer.shopping;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by azapata on 10/12/17.
 */
@Module
public class ShoppingCartModule {
    @Provides
    public ShoppingCartFragmentMVP.Presenter providesShoppingCartFragmentPresenter(ShoppingCartFragmentMVP.Model shModel) {
        return new ShoppingCartPresenter(shModel);
    }

    @Provides
    public ShoppingCartFragmentMVP.Model provideShoppingCartFragmentModel(Repository repository) {
        return new ShoppingCartModel(repository);
    }

    @Singleton
    @Provides
    public Repository provideRepo() {
        return new ShoppingRepository();
    }
}
