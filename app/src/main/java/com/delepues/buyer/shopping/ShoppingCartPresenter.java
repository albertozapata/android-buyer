package com.delepues.buyer.shopping;

import com.delepues.buyer.http.apimodel.Product;

import java.util.ArrayList;

import rx.Subscription;

/**
 * Created by azapata on 10/12/17.
 */

public class ShoppingCartPresenter implements ShoppingCartFragmentMVP.Presenter {
    private ShoppingCartFragmentMVP.View view;
    private Subscription subscription = null;
    private ShoppingCartFragmentMVP.Model model;

    public ShoppingCartPresenter(ShoppingCartFragmentMVP.Model model) {
        this.model = model;
    }

    @Override
    public void getProduct() {
        ArrayList<Product> result = model.getProduct();
        if (!result.isEmpty())
            view.showProducts(result);
        else
            view.errorEmpty("Error al obtener producto");
    }

    @Override
    public void rxUnsubscribe() {
        if (subscription != null) {
            if (!subscription.isUnsubscribed()) {
                subscription.unsubscribe();
            }
        }
    }

    @Override
    public void setView(ShoppingCartFragmentMVP.View view) {
        this.view = view;
    }

}
