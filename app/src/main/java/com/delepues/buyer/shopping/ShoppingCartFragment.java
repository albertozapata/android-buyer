package com.delepues.buyer.shopping;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.delepues.buyer.R;
import com.delepues.buyer.http.apimodel.Product;
import com.delepues.buyer.root.App;
import com.delepues.buyer.store.dialog.BuyDialogFragment2;

import java.util.ArrayList;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ShoppingCartFragment extends Fragment implements ShoppingCartFragmentMVP.View {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

//
//    @BindView(R.id.toolbar)
//    Toolbar toolbar;

    @BindView(R.id.list_shopping_cart)
    ListView listView;

    @BindView(R.id.button_buy_shopping)
    Button buy;

    @BindView(R.id.text_total_shopping)
    TextView totalShopping;

    @BindView(R.id.text_total_label)
    TextView totalLabel;

    @Inject
    ShoppingCartFragmentMVP.Presenter presenter;
    private ShoppingCartAdapter adapter;

    public ShoppingCartFragment() {
        // Required empty public constructor
    }

    public static ShoppingCartFragment newInstance(String param1, String param2) {
        ShoppingCartFragment fragment = new ShoppingCartFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getActivity().getApplication()).getComponent().inject(this);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_shopping_cart, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUpToolbar();
        presenter.setView(this);
//        final ArrayList<Product> list = new ArrayList<>();
//        list.add(new Product("", "Papas", 9.99, "", "1  Mc Doble Cheese, 1 orden de Papas, 1 Soda/ 16onz  ", null));
//        list.add(new Product("", "Agrandar Combo", 9.99, "", "1  Mc Doble Cheese, 1 orden de Papas, 1 Soda/ 16onz  ", null));
//        list.add(new Product("", "Cebollas", 9.99, "", "1  Mc Doble Cheese, 1 orden de Papas, 1 Soda/ 16onz  ", null));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                BuyDialogFragment2 buyDialogFragment = BuyDialogFragment2.newInstance(adapter.getDataSet().get(position), true);
                buyDialogFragment.show(fm, "fragment_edit_name");
            }
        });
        presenter.getProduct();
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.setView(this);
    }

    @Override
    public void showProducts(ArrayList<Product> products) {
        adapter = new ShoppingCartAdapter(products, getActivity());
        listView.setAdapter(adapter);
        double total = 0;
        for (Product product : products) {
            total += (product.qty * product.price);
        }
        if (total > 0) {
            totalShopping.setText(String.format(Locale.getDefault(), "%.2f", total));
        } else {
            totalLabel.setVisibility(View.INVISIBLE);
            totalShopping.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void errorEmpty(String s) {

    }

    @Override
    public void onResume() {
        super.onResume();
        if (adapter != null)
            presenter.getProduct();
    }

    private void setUpToolbar() {
//        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.title_landing_fragment);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
    }
}
