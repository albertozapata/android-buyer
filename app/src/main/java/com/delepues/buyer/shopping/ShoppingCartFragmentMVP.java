package com.delepues.buyer.shopping;

import com.delepues.buyer.http.apimodel.Product;

import java.util.ArrayList;

/**
 * Created by azapata on 10/12/17.
 */

public interface ShoppingCartFragmentMVP {
    interface View {
        void showProducts(ArrayList<Product> products);

        void errorEmpty(String s);
    }

    interface Presenter {
        void getProduct();

        void rxUnsubscribe();

        void setView(ShoppingCartFragmentMVP.View view);
    }

    interface Model {
        ArrayList<Product> getProduct();
    }
}
