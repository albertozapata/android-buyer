package com.delepues.buyer.shopping;

import com.delepues.buyer.http.apimodel.Product;
import com.delepues.buyer.http.apimodel.SectionDataModel;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public interface Repository {
    ArrayList<Product> getProducts();
}
