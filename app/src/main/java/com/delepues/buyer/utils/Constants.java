package com.delepues.buyer.utils;

/**
 * Created by azapata on 9/27/17.
 */

public class Constants {
    public static String latitude = "LATITUDE";
    public static String longitude = "LONGITUDE";
    public static String codeCountry = "CODE_COUNTRY";
    public static String category = "CATEGORY";
    public static String branch = "BRANCH";
    public static String product = "PRODUCT";
    public static String read = "READ";
    public static String address = "ADDRESS";
    public static String orders = "ORDERS";
    public static String buyerId = "BUYER_ID";
    public static String token = "TOKEN";
    public static String order = "ORDER";
    public static String user = "USER";
    public static String email = "EMAIL";
    public static String phone = "PHONE";
}
