package com.delepues.buyer.utils

import android.util.Base64
import android.util.Log
import android.widget.EditText
import android.widget.ImageView
import com.delepues.buyer.R
import com.squareup.picasso.Picasso
import retrofit2.HttpException
import java.io.UnsupportedEncodingException
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

/**
 * Created by azapata on 10/11/17.
 */

fun ImageView.loadUrl(url: String) {
    Picasso.with(this.context).load(url).placeholder(this.context.resources.getDrawable(R.drawable.logo_placeholder)).into(this)
}

fun ImageView.loadUrl(url: Int) {
    Picasso.with(this.context).load(url).into(this)
}

fun String.getSHA(text: String): String? {
    val md: MessageDigest?
    try {
        md = MessageDigest.getInstance("SHA-256")
        md!!.update(text.toByteArray(charset("UTF-8"))) // Change this to "UTF-16" if needed
        val digest = md.digest()
        return Base64.encodeToString(digest, Base64.DEFAULT)
    } catch (e: NoSuchAlgorithmException) {
        e.printStackTrace()
        return "error"
    } catch (e: UnsupportedEncodingException) {
        e.printStackTrace()
        return "error"
    }
}

