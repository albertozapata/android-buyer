package com.delepues.buyer;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.delepues.buyer.category.StoresFragment;
import com.delepues.buyer.history.HistoryFragment;
import com.delepues.buyer.http.apimodel.Branch;
import com.delepues.buyer.http.apimodel.Category;
import com.delepues.buyer.http.apimodel.Product;
import com.delepues.buyer.landing.LandingFragment;
import com.delepues.buyer.login.LoginFragment;
import com.delepues.buyer.order.OrderActivity;
import com.delepues.buyer.profile.ProfileFragment;
import com.delepues.buyer.shopping.ShoppingCartFragment;
import com.delepues.buyer.store.ProductsFragment;
import com.delepues.buyer.store.StoreFragment;
import com.delepues.buyer.utils.Constants;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;


public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    Drawer result;
    private PermissionListener permissionlistener;
    GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setUpToolbar();

        permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                getLocation();
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                Toast.makeText(MainActivity.this, "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }
        };

        TedPermission.with(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(android.Manifest.permission.ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION)
                .check();

        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.color.primary)
                .addProfiles(
                        new ProfileDrawerItem().withName("azapata Garcia").withEmail("f.cjulio5@gmail.com").withIcon(getResources().getDrawable(R.drawable.ic_mic_black_vector))
                )
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {
                        return false;
                    }
                })
                .build();

        PrimaryDrawerItem item1 = new PrimaryDrawerItem().withIdentifier(1).withName("Principal").withIcon(FontAwesome.Icon.faw_home);
        PrimaryDrawerItem item2 = new PrimaryDrawerItem().withIdentifier(2).withName("Pedidos").withIcon(FontAwesome.Icon.faw_list);
        PrimaryDrawerItem item3 = new PrimaryDrawerItem().withIdentifier(3).withName("Carrito").withIcon(FontAwesome.Icon.faw_shopping_cart);
        PrimaryDrawerItem item4 = new PrimaryDrawerItem().withIdentifier(4).withName("Perfil").withIcon(FontAwesome.Icon.faw_user);
        PrimaryDrawerItem item5 = new PrimaryDrawerItem().withIdentifier(5).withName("Perfil").withIcon(FontAwesome.Icon.faw_user);

        result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(headerResult)
                .addDrawerItems(
                        item1,
                        item2,
                        item3,
                        item4
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        result.closeDrawer();
                        Fragment fragment = null;
                        String tag = "";
                        switch (position) {
                            case 1:
                                fragment = LandingFragment.newInstance(Prefs.getString(Constants.codeCountry, "x"));
                                tag = "Landing";
                                break;
                            case 2:
                                fragment = HistoryFragment.newInstance("");
                                break;
                            case 3:
                                fragment = ShoppingCartFragment.newInstance("", "");
                                break;
                            case 4:
                                fragment = new ProfileFragment();
                                break;
                            case 5:
                                Intent intent = new Intent(MainActivity.this, OrderActivity.class);
                                startActivity(intent);
                                break;
                        }

                        if (fragment != null) {
                            FragmentManager fragmentManager = getSupportFragmentManager();
                            FragmentTransaction transaction = fragmentManager.beginTransaction();
                            transaction.replace(R.id.fragment2, fragment, tag);
                            transaction.addToBackStack(null);
                            transaction.commitAllowingStateLoss();
                        }

                        return true;
                    }
                })
                .build();
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

//        String token = Prefs.getString(Constants.token, "");
//        if (token.isEmpty()) {
//            result.getActionBarDrawerToggle().setDrawerIndicatorEnabled(false);
//        } else {
//            result.getActionBarDrawerToggle().setDrawerIndicatorEnabled(true);
//        }
        result.setSelection(-1);

//        DrawerLayout drawerLayout = result.getDrawerLayout();
//        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.e("DELEPUES", "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onConnected(Bundle arg0) {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(10000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                getCodeCountry(location);
            }
        });
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();
    }

    private void getLocation() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();

        mGoogleApiClient.connect();
//        DrawerLayout drawerLayout = result.getDrawerLayout();
//        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    private void setUpToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.title_landing_fragment));
    }

    private void getCodeCountry(Location location) {
        try {
            Geocoder geo = new Geocoder(this.getApplicationContext(), Locale.getDefault());
            List<Address> addresses = geo.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            if (!addresses.isEmpty()) {
                if (addresses.size() > 0) {
                    Prefs.putDouble(Constants.latitude, location.getLatitude());
                    Prefs.putDouble(Constants.longitude, location.getLongitude());
                    saveCountryCode(addresses.get(0).getCountryCode());
                }
            }
        } catch (Exception e) {
            Toast.makeText(this, "No Location Name Found", Toast.LENGTH_LONG).show();
        }
    }

    private void saveCountryCode(String code) {
        String[] codes = getResources().getStringArray(R.array.country_code);
        for (String s : codes) {
            String[] text = s.split(",");
            if (text[1].equals(code)) {
                Prefs.putString(Constants.codeCountry, text[0]);
            }
        }
        result.setSelection(1);
        mGoogleApiClient.disconnect();
    }

    public void changeFragment(String frag, Object object) {
        Fragment fragment = null;
        switch (frag) {
            case "Stores":
                fragment = StoresFragment.newInstance(Prefs.getDouble(Constants.latitude, 0), Prefs.getDouble(Constants.latitude, 0), ((Category) object));
                break;

            case "Branch":
                fragment = StoreFragment.newInstance(((Branch) object));
                break;

            case "Products":
                fragment = ProductsFragment.Companion.newInstance((ArrayList<Product>) object);
                break;
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment2, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        String token = Prefs.getString(Constants.token, "");
        if (result != null) {
            if (token.isEmpty())
                result.getActionBarDrawerToggle().setDrawerIndicatorEnabled(false);
            else {
                clearBackstack();
                result.getActionBarDrawerToggle().setDrawerIndicatorEnabled(true);
                result.setSelection(1);
            }
        }
    }

    @Override
    public void onBackPressed() {
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragment2);
        if (f instanceof LandingFragment)
            this.finish();
        else
            super.onBackPressed();
    }

    public void clearBackstack() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        if (count > 0) {
            FragmentManager.BackStackEntry entry = getSupportFragmentManager().getBackStackEntryAt(
                    0);
            getSupportFragmentManager().popBackStack(entry.getId(),
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
            getSupportFragmentManager().executePendingTransactions();
        }
    }
}
