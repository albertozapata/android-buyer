package com.delepues.buyer.history;

import com.delepues.buyer.http.apimodel.Category;
import com.delepues.buyer.http.apimodel.Order;
import com.delepues.buyer.landing.LandingFragmentMVP;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public class HistoryModel implements HistoryFragmentMVP.Model {
    private Repository repository;

    public HistoryModel(Repository repository) {
        this.repository = repository;
    }

    @Override
    public Observable<ArrayList<Order>> getOrders(String buyerId) {
        //return repository.getResultsFromNetwork(country);
        return repository.getResultsFromLocal(buyerId);
    }
}
