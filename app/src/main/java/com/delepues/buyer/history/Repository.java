package com.delepues.buyer.history;

import com.delepues.buyer.http.apimodel.Category;
import com.delepues.buyer.http.apimodel.Order;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public interface Repository {
    Observable<ArrayList<Order>> getResultsFromNetwork(String buyerId);
    Observable<ArrayList<Order>> getResultsFromLocal(String buyerId);
}
