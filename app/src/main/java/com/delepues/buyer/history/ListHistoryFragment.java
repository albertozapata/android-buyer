package com.delepues.buyer.history;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import com.delepues.buyer.MainActivity;
import com.delepues.buyer.R;
import com.delepues.buyer.http.apimodel.Category;
import com.delepues.buyer.http.apimodel.Order;
import com.delepues.buyer.landing.HistoryAdapter;
import com.delepues.buyer.landing.LandingAdapter;
import com.delepues.buyer.order.OrderActivity;
import com.delepues.buyer.utils.Constants;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListHistoryFragment extends Fragment {


    @BindView(R.id.recycler_history)
    RecyclerView recyclerView;
    @Inject
    HistoryFragmentMVP.Presenter presenter;
    ArrayList<Order> orders = new ArrayList<>();

    public ListHistoryFragment() {
        // Required empty public constructor
    }

    public static ListHistoryFragment newInstance(ArrayList<Order> orders) {
        ListHistoryFragment fragment = new ListHistoryFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(Constants.orders, orders);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            orders = getArguments().getParcelableArrayList(Constants.orders);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_list_history, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getActivity(), resId);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutAnimation(animation);

        RecyclerView.Adapter adapter = new HistoryAdapter(orders, getActivity(), new HistoryAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(@NotNull Order order) {
                Intent intent = new Intent(getActivity(), OrderActivity.class);
                intent.putExtra(Constants.order,order);
                startActivity(intent);
            }
        });
        recyclerView.setAdapter(adapter);
        recyclerView.getAdapter().notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();
    }
}
