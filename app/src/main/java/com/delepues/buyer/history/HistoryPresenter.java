package com.delepues.buyer.history;

import com.delepues.buyer.http.apimodel.Category;
import com.delepues.buyer.http.apimodel.Order;
import com.delepues.buyer.landing.LandingFragmentMVP;

import java.util.ArrayList;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by azapata on 10/12/17.
 */

public class HistoryPresenter implements HistoryFragmentMVP.Presenter {
    private HistoryFragmentMVP.View view;
    private Subscription subscription = null;
    private HistoryFragmentMVP.Model model;
    private ArrayList<Order> orders = new ArrayList<>();

    public HistoryPresenter(HistoryFragmentMVP.Model model) {
        this.model = model;
    }


    @Override
    public void getOrders(String buyerId) {
        subscription = model.getOrders(buyerId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ArrayList<Order>>() {
                    @Override
                    public void onCompleted() {
                        if (!orders.isEmpty())
                            view.showOrders(orders);
                        else
                            view.errorEmpty();
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.errorOrders(e.getMessage());
                    }

                    @Override
                    public void onNext(ArrayList<Order> orderArrayList) {
                        orders = orderArrayList;
                    }
                });
    }

    @Override
    public void rxUnsubscribe() {
        if (subscription != null) {
            if (!subscription.isUnsubscribed()) {
                subscription.unsubscribe();
            }
        }
    }

    @Override
    public void setView(HistoryFragmentMVP.View view) {
        this.view = view;
    }
}
