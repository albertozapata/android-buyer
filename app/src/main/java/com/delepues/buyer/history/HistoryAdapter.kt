package com.delepues.buyer.landing

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.delepues.buyer.R
import com.delepues.buyer.http.apimodel.Order
import com.delepues.buyer.utils.loadUrl
import com.makeramen.roundedimageview.RoundedImageView
import java.util.*
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView


/**
 * Created by azapata on 10/10/17.
 */

class HistoryAdapter(private val dataSet: ArrayList<Order>, private val context: Context, private val listener: HistoryAdapter.OnItemClickListener) : RecyclerView.Adapter<HistoryAdapter.MyViewHolder>() {

    //var vectorList = intArrayOf(R.drawable.super_mercado_vector, R.drawable.farmacia_vector, R.drawable.restaurante_vector, R.drawable.tienda_vector)

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        internal var name: TextView = itemView.findViewById<View>(R.id.text_item_history_name) as TextView
        internal var price: TextView = itemView.findViewById<View>(R.id.text_item_history_status) as TextView
        internal var status: TextView = itemView.findViewById<View>(R.id.text_item_history_price) as TextView
        internal var logo: CircleImageView = itemView.findViewById<View>(R.id.image_item_store2) as CircleImageView

        fun bind(item: Order, listener: OnItemClickListener, total: Double) {
            name.text = item.branch_name
            price.text = String.format("C$ %.2f", total)
            status.text = item.state_name
            logo.loadUrl(item.branch_image_url)
            itemView.setOnClickListener {
                listener.onItemClick(item)
            }
            //imageViewIcon.setImageDrawable(context.resources.getDrawable(vectorList[listPosition]))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_history, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, listPosition: Int) {

        holder.bind(dataSet[listPosition], listener, getTotal(dataSet[listPosition]))
    }

    override fun getItemCount(): Int = dataSet.size

    fun getTotal(order: Order): Double {
        var total = 0.0
        order.details.forEach {
            total += it.price * it.quantity
        }
        return total
    }

    interface OnItemClickListener {
        fun onItemClick(item: Order)
    }
}
