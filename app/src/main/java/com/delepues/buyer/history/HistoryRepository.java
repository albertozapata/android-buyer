package com.delepues.buyer.history;

import com.delepues.buyer.http.BuyerApiService;
import com.delepues.buyer.http.apimodel.Order;
import com.delepues.buyer.http.apimodel.OrderDetail;
import com.delepues.buyer.http.apimodel.OrderOption;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public class HistoryRepository implements Repository {
    BuyerApiService buyerApiService;

    public HistoryRepository(BuyerApiService buyerApiService) {
        this.buyerApiService = buyerApiService;
    }

    @Override
    public Observable<ArrayList<Order>> getResultsFromNetwork(String buyerId) {
        return buyerApiService.orders(buyerId);
    }

    @Override
    public Observable<ArrayList<Order>> getResultsFromLocal(String buyerId) {
        ArrayList<Order> orders = new ArrayList<>();
        ArrayList<OrderDetail> details = new ArrayList<>();
        ArrayList<OrderOption> options = new ArrayList<>();

        Order order = new Order();
        order.id = "cdd35a25-3320-48b2-982a-830ecd608b95";
        order.buyer_id = "36fb3e1d-998d-46d2-8d74-2d099576215f";
        order.store_branch_id = "b805f92b-28e7-4c20-ab4b-f5494d7d142f";
        order.driver_id = "1";
        order.order_state_id = "cbd18346-81de-4437-a87c-5f718bddcdb1";
        order.pick_up_time = "00:00 PM";
        order.delivery_time = "00:00 PM";
        order.branch_name = "My Store Branch";
        order.branch_image_url = "https://lh3.googleusercontent.com/-GGHvgo2I0FU/AAAAAAAAAAI/AAAAAAAAAO4/u4njwnhi0Rs/s0-c-k-no-ns/photo.jpg";
        order.state_name = "PENDING";
        order.in_progress = true;

        OrderDetail orderDetail = new OrderDetail();
        orderDetail.name = "Calamari";
        orderDetail.price = 10.00;
        orderDetail.quantity = 2;
        orderDetail.image_url = "http://www.pizzachicago.com/images/import/wp/wp-content/uploads/2011/01/calamari.jpg";

        OrderOption orderOption = new OrderOption();
        orderOption.name = "detalle 1";
        orderOption.price = 10.30;
        orderOption.option_detail_id = "297e26d8-b37a-4dcc-999c-fdf82a0eefe9";
        options.add(orderOption);
        options.add(orderOption);

        orderDetail.options = options;
        details.add(orderDetail);
        order.details = details;

        orders.add(order);
        orders.add(order);

        details = new ArrayList<>();
        options = new ArrayList<>();

        order = new Order();
        order.id = "cdd35a25-3320-48b2-982a-830ecd608b95";
        order.buyer_id = "36fb3e1d-998d-46d2-8d74-2d099576215f";
        order.store_branch_id = "b805f92b-28e7-4c20-ab4b-f5494d7d142f";
        order.driver_id = "1";
        order.order_state_id = "cbd18346-81de-4437-a87c-5f718bddcdb1";
        order.pick_up_time = "00:00 PM";
        order.delivery_time = "00:00 PM";
        order.branch_name = "My Store Branch";
        order.branch_image_url = "https://lh3.googleusercontent.com/-GGHvgo2I0FU/AAAAAAAAAAI/AAAAAAAAAO4/u4njwnhi0Rs/s0-c-k-no-ns/photo.jpg";
        order.state_name = "DELIVERED";
        order.in_progress = false;

        orderDetail = new OrderDetail();
        orderDetail.name = "Calamari";
        orderDetail.price = 10.00;
        orderDetail.quantity = 2;
        orderDetail.image_url = "http://www.pizzachicago.com/images/import/wp/wp-content/uploads/2011/01/calamari.jpg";

        orderOption = new OrderOption();
        orderOption.name = "detalle 1";
        orderOption.price = 10.30;
        orderOption.option_detail_id = "297e26d8-b37a-4dcc-999c-fdf82a0eefe9";
        options.add(orderOption);
        options.add(orderOption);

        orderDetail.options = options;
        details.add(orderDetail);
        details.add(orderDetail);
        details.add(orderDetail);

        order.details = details;

        orders.add(order);
        orders.add(order);

        return Observable.just(orders);
    }
}
