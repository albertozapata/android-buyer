package com.delepues.buyer.history;

import com.delepues.buyer.http.BuyerApiService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by azapata on 10/12/17.
 */
@Module
public class HistoryModule {
    @Provides
    public HistoryFragmentMVP.Presenter providesHistoryFragmentPresenter(HistoryFragmentMVP.Model landingModel) {
        return new HistoryPresenter(landingModel);
    }

    @Provides
    public HistoryFragmentMVP.Model provideHistoryFragmentModel(Repository repository) {
        return new HistoryModel(repository);
    }

    @Singleton
    @Provides
    public Repository provideRepo(BuyerApiService buyerApiService) {
        return new HistoryRepository(buyerApiService);
    }
}
