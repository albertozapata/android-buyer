package com.delepues.buyer.history;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.delepues.buyer.R;
import com.delepues.buyer.http.apimodel.Order;
import com.delepues.buyer.landing.LandingFragmentMVP;
import com.delepues.buyer.order.InvoiceFragment;
import com.delepues.buyer.order.StatusFragment;
import com.delepues.buyer.order.ViewPagerAdapter;
import com.delepues.buyer.root.App;
import com.delepues.buyer.utils.Constants;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryFragment extends Fragment implements HistoryFragmentMVP.View {

    private String buyerId;

    @BindView(R.id.tabs)
    TabLayout tabLayout;

    @BindView(R.id.view_pager_order)
    ViewPager viewPager;

    @Inject
    HistoryFragmentMVP.Presenter presenter;

    public HistoryFragment() {
        // Required empty public constructor
    }

    public static HistoryFragment newInstance(String buyerId) {
        HistoryFragment fragment = new HistoryFragment();
        Bundle args = new Bundle();
        args.putString(Constants.buyerId, buyerId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getActivity().getApplication()).getComponent().inject(this);
        if (getArguments() != null) {
            buyerId = getArguments().getString(Constants.buyerId);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.getOrders(buyerId);
    }

    @Override
    public void showOrders(ArrayList<Order> orders) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        ArrayList<Order> orders1 = new ArrayList<>();
        ArrayList<Order> orders2 = new ArrayList<>();

        for (Order order : orders) {
            if (order.state_name.equals("DELIVERED"))
                orders1.add(order);
            else
                orders2.add(order);
        }

        adapter.addFragment(ListHistoryFragment.newInstance(orders2), "ACTIVAS");
        adapter.addFragment(ListHistoryFragment.newInstance(orders1), "FINALIZADAS");
        viewPager.setAdapter(adapter);

        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void errorOrders(String s) {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void errorEmpty() {

    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.setView(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.rxUnsubscribe();
    }
}
