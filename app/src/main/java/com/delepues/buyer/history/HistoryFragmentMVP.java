package com.delepues.buyer.history;

import com.delepues.buyer.http.apimodel.Order;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public interface HistoryFragmentMVP {
    interface View {
        void showOrders(ArrayList<Order> orders);

        void errorOrders(String s);

        void showProgress();

        void hideProgress();

        void errorEmpty();
    }

    interface Presenter {
        void getOrders(String buyerId);

        void rxUnsubscribe();

        void setView(HistoryFragmentMVP.View view);
    }

    interface Model {
        Observable<ArrayList<Order>> getOrders(String buyerId);
    }
}
