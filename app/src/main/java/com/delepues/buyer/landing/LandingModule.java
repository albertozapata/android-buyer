package com.delepues.buyer.landing;

import com.delepues.buyer.http.BuyerApiService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by azapata on 10/12/17.
 */
@Module
public class LandingModule {
    @Provides
    public LandingFragmentMVP.Presenter providesLandingFragmentPresenter(LandingFragmentMVP.Model landingModel) {
        return new LandingPresenter(landingModel);
    }

    @Provides
    public LandingFragmentMVP.Model provideLandingFragmentModel(Repository repository) {
        return new LandingModel(repository);
    }

    @Singleton
    @Provides
    public Repository provideRepo(BuyerApiService buyerApiService) {
        return new LandingRepository(buyerApiService);
    }
}
