package com.delepues.buyer.landing;

import com.delepues.buyer.http.apimodel.Category;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public class LandingModel implements LandingFragmentMVP.Model {
    private Repository repository;

    public LandingModel(Repository repository) {
        this.repository = repository;
    }

    @Override
    public Observable<ArrayList<Category>> getCategories(String country) {
        //return repository.getResultsFromNetwork(country);
        return repository.getResultsFromNetwork(country);
    }
}
