package com.delepues.buyer.landing;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import com.delepues.buyer.MainActivity;
import com.delepues.buyer.R;
import com.delepues.buyer.http.apimodel.Category;
import com.delepues.buyer.root.App;
import com.delepues.buyer.utils.Constants;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LandingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LandingFragment extends Fragment implements LandingFragmentMVP.View {
    private String countryCode;

    @BindView(R.id.recycler_category)
    RecyclerView recyclerView;

    @Inject
    LandingFragmentMVP.Presenter presenter;

    public LandingFragment() {
        // Required empty public constructor
    }

    public static LandingFragment newInstance(String countryCode) {
        LandingFragment fragment = new LandingFragment();
        Bundle args = new Bundle();
        args.putString(Constants.codeCountry, countryCode);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getActivity().getApplication()).getComponent().inject(this);
        if (getArguments() != null) {
            countryCode = getArguments().getString(Constants.codeCountry);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_landing, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.getCategories(countryCode);

        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getActivity(), resId);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutAnimation(animation);
        //showCategories(null);
    }

    @Override
    public void showCategories(ArrayList<Category> categories) {
        hideProgress();
        RecyclerView.Adapter adapter = new LandingAdapter(categories, getActivity(), new LandingAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(@NotNull Category item) {
                ((MainActivity)getActivity()).changeFragment("Stores",item);
            }
        });
        recyclerView.setAdapter(adapter);
        recyclerView.getAdapter().notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();
    }

    @Override
    public void errorCategories(String s) {
        hideProgress();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void errorEmpty() {

    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.setView(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.rxUnsubscribe();
    }

}
