package com.delepues.buyer.landing

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.delepues.buyer.R
import com.delepues.buyer.http.apimodel.Category
import com.delepues.buyer.utils.loadUrl
import com.makeramen.roundedimageview.RoundedImageView
import java.util.*
import com.squareup.picasso.Picasso


/**
 * Created by azapata on 10/10/17.
 */

class LandingAdapter(private val dataSet: ArrayList<Category>, private val context: Context, private val listener: LandingAdapter.OnItemClickListener) : RecyclerView.Adapter<LandingAdapter.MyViewHolder>() {

    //var vectorList = intArrayOf(R.drawable.super_mercado_vector, R.drawable.farmacia_vector, R.drawable.restaurante_vector, R.drawable.tienda_vector)

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        internal var textViewName: TextView = itemView.findViewById<View>(R.id.text_item_category) as TextView
        internal var textViewVersion: TextView = itemView.findViewById<View>(R.id.text_item_desc) as TextView
        internal var imageViewIcon: RoundedImageView = itemView.findViewById<View>(R.id.image_item_background) as RoundedImageView

        fun bind(item: Category, listener: OnItemClickListener) {
            textViewName.text = item.name
            textViewVersion.text = "21"
            imageViewIcon.loadUrl(item.image_url)
            itemView.setOnClickListener {
                listener.onItemClick(item)
            }
            //imageViewIcon.setImageDrawable(context.resources.getDrawable(vectorList[listPosition]))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_categories, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, listPosition: Int) {
        holder.bind(dataSet[listPosition], listener)
    }

    override fun getItemCount(): Int = dataSet.size

    interface OnItemClickListener {
        fun onItemClick(item: Category)
    }
}
