package com.delepues.buyer.landing;

import com.delepues.buyer.http.apimodel.Category;

import java.util.ArrayList;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by azapata on 10/12/17.
 */

public class LandingPresenter implements LandingFragmentMVP.Presenter {
    private LandingFragmentMVP.View view;
    private Subscription subscription = null;
    private LandingFragmentMVP.Model model;
    private ArrayList<Category> categories = new ArrayList<>();

    public LandingPresenter(LandingFragmentMVP.Model model) {
        this.model = model;
    }

    @Override
    public void getCategories(String country) {
        subscription = model.getCategories(country).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ArrayList<Category>>() {
                    @Override
                    public void onCompleted() {
                        if (!categories.isEmpty())
                            view.showCategories(categories);
                        else
                            view.errorEmpty();
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.errorCategories(e.getMessage());
                    }

                    @Override
                    public void onNext(ArrayList<Category> categoryList) {
                        categories = categoryList;
                    }
                });
    }

    @Override
    public void rxUnsubscribe() {
        if (subscription != null) {
            if (!subscription.isUnsubscribed()) {
                subscription.unsubscribe();
            }
        }
    }

    @Override
    public void setView(LandingFragmentMVP.View view) {
        this.view = view;
    }
}
