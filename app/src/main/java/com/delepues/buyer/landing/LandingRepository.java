package com.delepues.buyer.landing;

import com.delepues.buyer.http.BuyerApiService;
import com.delepues.buyer.http.apimodel.Category;
import com.delepues.buyer.utils.Constants;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public class LandingRepository implements Repository {
    BuyerApiService buyerApiService;

    public LandingRepository(BuyerApiService buyerApiService) {
        this.buyerApiService = buyerApiService;
    }

    @Override
    public Observable<ArrayList<Category>> getResultsFromNetwork(String country) {
        String token = Prefs.getString(Constants.token, "");
        if (token.isEmpty()) {
            token = "deb03f8e-8339-4410-89a2-671c2b25edce";
        }
        return buyerApiService.storeCategories(token, country);
    }

    @Override
    public Observable<ArrayList<Category>> getResultsFromLocal(String country) {
        ArrayList<Category> categories = new ArrayList<>();
        categories.add(new Category("1", "Farmacias", "https://i.imgur.com/QFwsoff.png"));
        categories.add(new Category("2", "Restaurantes", "https://i.imgur.com/6Rs4ZSp.png"));
        categories.add(new Category("3", "Supermercados", "https://i.imgur.com/HRVCGgM.png"));
        categories.add(new Category("4", "Tiendas", "https://i.imgur.com/EOY53qp.png"));

        return Observable.just(categories);
    }
}
