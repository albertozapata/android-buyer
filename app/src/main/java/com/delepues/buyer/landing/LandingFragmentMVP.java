package com.delepues.buyer.landing;

import com.delepues.buyer.http.apimodel.Category;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public interface LandingFragmentMVP {
    interface View {
        void showCategories(ArrayList<Category> categories);

        void errorCategories(String s);

        void showProgress();

        void hideProgress();

        void errorEmpty();
    }

    interface Presenter {
        void getCategories(String country);

        void rxUnsubscribe();

        void setView(LandingFragmentMVP.View view);
    }

    interface Model {
        Observable<ArrayList<Category>> getCategories(String country);
    }
}
