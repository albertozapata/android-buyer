package com.delepues.buyer.landing;

import com.delepues.buyer.http.apimodel.Category;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public interface Repository {
    Observable<ArrayList<Category>> getResultsFromNetwork(String country);
    Observable<ArrayList<Category>> getResultsFromLocal(String country);
}
